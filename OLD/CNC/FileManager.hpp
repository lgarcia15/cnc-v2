#ifndef _FILEMANAGER_H_
#define _FILEMANAGER_H_
#include <Arduino.h>
#include <SPI.h>
#include <SdFat.h>


#define MaxFiles 100

extern int nFiles;

void FMBegin(int pin, String dir);
void getFiles(String * files);
int fileNum();
void rebootFM(int pin, String dir);
int getLines(String file);

#endif
