/* 
 Mini CNC Plotter firmware, based in TinyCNC https://github.com/MakerBlock/TinyCNC-Sketches
 Send GCODE to this Sketch using gctrl.pde https://github.com/damellis/gctrl
 Convert SVG to GCODE with MakerBot Unicorn plugin for Inkscape available here https://github.com/martymcguire/inkscape-unicorn
 
 More information about the Mini CNC Plotter here (german, sorry): http://www.makerblog.at/2015/02/projekt-mini-cnc-plotter-aus-alten-cddvd-laufwerken/
  */

#include <Servo.h>
#include <Stepper.h>
#include <SimpleEncoder.h>
#include <TimerOne.h>
#include <BasicStepperDriver.h>
#include <GCodeParser.h>
#include <PinChangeInterrupt.h>
#include <InputDebounce.h>
#include "Rotary.h"
#include "Pantalla.hpp" // Archivo para controlar la pantalla
#include "FileManager.hpp" // Archivo para controlar la SD

#define FINX1 A9
#define FINX2 A10
#define FINY1 A11
#define FINY2 A12
#define FINZ A13

bool esFinX1 = false;
bool esFinX2 = false;
bool esFinY1 = false;
bool esFinY2 = false;
bool esFinZ = false;


//Declaro Encoder
#define encCLK A14
#define encDT A15
#define btnENC A8
#define btnStop 46
Rotary r = Rotary(encCLK, encDT);
bool ok = false;


//BEEPER
#define beeper 44
//GCODE PARSER
GCodeParser GCode = GCodeParser();


//Memoria
int pos = -1;
int posAnt = pos;
String currentFile;
String dir = "/GCODES/";
int fileLines = 0;
SdFat sdMain;
File printFile;

//Pantalla
int currentMenu = 0;
bool need_update;
bool force_update;
int posRel = -10;
int posRelAnt = -10;
int stopBtn = 1;
int stopBtnAnt = 1;
int boton = 1;
int botonAnt = 1;
float printProgress;
float oldProgress;
int currentLine = 0;




//VARIABLES NECESARIAS MOVIMIENTO

float feedRate = 10.00;
bool absMode = true;
bool useMM = true;
int spindleSpeed;
bool usilloPrendido = false;
#define PWM_USILLO 2
#define ZCAL_TEST 24


//Timer
unsigned long timeR = 0;

//BOTONES
#define BUTTON_DEBOUNCE_DELAY   20
#define BUTTON_MAS 26
#define BUTTON_MENOS 28
static InputDebounce buttonMas; // not enabled yet, setup has to be called first, see setup() below
static InputDebounce buttonMenos;

String files[MaxFiles];
#define LINE_BUFFER_LENGTH 512


// Configuracion de distancias
#define STEPS_PER_REV_X 1600
#define STEPS_PER_REV_Y 1600
#define STEPS_PER_REV_Z 1600
#define REV_PER_MM_X 0.125
#define REV_PER_MM_Y 0.125
#define REV_PER_MM_Z 0.125

//MAPA DE Z
int puntosCal = 2;
float maxX = 0;
float maxY = 0;
float maxXDef = 0;
float maxYDef = 0;
float offsetZ;
int cantPuntDef;
//float **table;


// Motores Paso a Paso

//EJE Z
#define EN_Z 5
#define STEP_Z 4
#define DIR_Z 3
BasicStepperDriver ejeZ(STEPS_PER_REV_Z, DIR_Z, STEP_Z);
//AccelStepper ejeZ(1, STEP_Z, DIR_Z);


//EJE X
#define EN_X 8
#define STEP_X 7
#define DIR_X 6
BasicStepperDriver ejeX(STEPS_PER_REV_X, DIR_X, STEP_X);
//AccelStepper ejeX(1, STEP_X, DIR_X);

//EJE Y
#define EN_Y 9
#define STEP_Y 10
#define DIR_Y 11
BasicStepperDriver ejeY(STEPS_PER_REV_Y, DIR_Y, STEP_Y);
//AccelStepper ejeY(1, STEP_Y, DIR_Y);

int VELMOTPAP = 50;

/* Structures, global variables    */
struct point { 
  float x; 
  float y; 
  float z; 
};

//VARIABLES CALIBRACIÓN
int ejeCal;
int multCal;
struct point proyCero;
struct point actuatorPosRel;

// Current position of plothead
struct point actuatorPos;

//  Drawing settings, should be OK
float StepInc = 1;
int StepDelay = 0;
int LineDelay = 50;
int penDelay = 50;

// Drawing robot limits, in mm
// OK to start with. Could go up to 50 mm if calibrated well. 
float Xmin = 0;
float Xmax = 40;
float Ymin = 0;
float Ymax = 40;
float Zmin = 0;
float Zmax = 1;

float Xpos = Xmin;
float Ypos = Ymin;
float Zpos = Zmax;



// Set to true to get debug output.
boolean verbose = false;

//  Needs to interpret 
//  G1 for moving
//  G4 P300 (wait 150ms)
//  M300 S30 (pen down)
//  M300 S50 (pen up)
//  Discard anything with a (
//  Discard any other command!

//Enum Estados

enum Estados
{
  None,
  Init,
  Home,
  SelectFile,
  Calibration,
  Printing,
  SiNo,
  MapaZ
};

Estados State;
Estados StateAnt;
Estados StateDeb;

void buttonMas_pressedCallback(uint8_t pinIn)
{
  if(State == Calibration) moverEje(ejeCal, multCal, true);

  if(State == MapaZ && posRel == 3 && posRel < 999)
  {
    puntosCal++;
    need_update = true;
  }
  
}

void buttonMenos_pressedCallback(uint8_t pinIn)
{
  if(State == Calibration) moverEje(ejeCal, multCal, false);
  if(State == MapaZ && posRel == 3 && posRel > 1)
  {
    puntosCal--;
    need_update = true;
  }
}

void changeState(Estados sta)
{
  if(State == sta)
  {
    return;
  }
  
  switch(sta)
  {
    case Init:
      currentMenu = -1;
      break;
    case Home:
      currentMenu = MENUHOME;
      break;
    case SelectFile:
      currentMenu = 4;
      break;
    case Calibration:
      currentMenu = MENUCALIBRATION;
      break;
    case Printing:
      currentMenu = MENUPRINTING;
      break;
    case SiNo:
      currentMenu = MENUSINO;
    case MapaZ:
      currentMenu = MENUMAPAZ;
    default:
      break;  
  }
  posRelAnt = posRel;
  posRel = -10;
  StateAnt = State;
  State = sta;
  need_update = true;
  return;
}

void moverEje(int eje, int mult, bool cw)
{
  int multiplicador;
  int sentido = 0;
#ifdef DEBUG  
  Serial.println(F("Movi"));
#endif
  if(cw) sentido = 1;
  if(!cw) sentido = -1;
  
  if(mult == 1)
  {
    multiplicador = 1;
  }
  if(mult == 2)
  {
    multiplicador = 10;
  }
  if(mult == 3)
  {
    multiplicador = 30;
  }
  if(eje == 1)
  {
    if(cw == true && digitalRead(esFinX2) == LOW)
    {
      return;
    }
    if(cw == false && digitalRead(esFinX1) == LOW)
    {
      return;
    }
#ifdef DEBUG
    Serial.println(F("EJE X MOVER"));
#endif
    float mm = 1 * multiplicador;
    float sent = mm * sentido;
    float rev =  sent * REV_PER_MM_X;
    long steps = rev * STEPS_PER_REV_X;
#ifdef DEBUG
    Serial.println(steps);
#endif
    ejeX.move(steps);
    updateActuatorPos(actuatorPos.x + sent, actuatorPos.y, actuatorPos.z, false);
  }
  if(eje == 2)
  {
    if(cw == true && digitalRead(esFinY1) == LOW)
    {
      return;
    }
    if(cw == false && digitalRead(esFinY2) == LOW)
    {
      return;
    }
    float mm = 1 * multiplicador;
    float sent = mm * sentido;
    float rev = sent * REV_PER_MM_Y;
    float steps = rev * STEPS_PER_REV_Y;
    ejeY.move(steps);
    updateActuatorPos(actuatorPos.x, actuatorPos.y + sent, actuatorPos.z, false);
  }
  if(eje == 3)
  {
    if(digitalRead(esFinZ) == LOW)
    {
      return;
    }
    float mm = 1 * multiplicador;
    float sent = mm * sentido;
    float rev = sent * REV_PER_MM_Z;
    float steps = rev * STEPS_PER_REV_Z;
    ejeZ.move(steps);
    updateActuatorPos(actuatorPos.x, actuatorPos.y , actuatorPos.z + sent, false);
  }
}

void(* resetFunc) (void) = 0;//declare reset function at address 0


void goHome()
{
  long moverX = 99999999;
  long moverY = 99999999;
  long moverZ = 99999999;


  int oldSpeed = VELMOTPAP;

  VELMOTPAP = 4000;
  if(!esFinX1 && !esFinY1)
  {
    drawLine(-99999999,-99999999, actuatorPos.z);
    updateActuatorPos(0, 0, 0, false);
    VELMOTPAP = oldSpeed;
    return;  
    
  }
  
  if(!esFinY1)
  {
    drawLine(actuatorPos.x,-99999999, actuatorPos.z);
  }

  if(!esFinX1)
  {
    drawLine(-99999999, actuatorPos.y, actuatorPos.z);
  }
  
  updateActuatorPos(0, 0, 0, false);  
  VELMOTPAP = oldSpeed;
  
}

int getLado()
{
      int lado = 0;
      unsigned char result = r.process();
      if(result)
      {
        if(result == DIR_CW)
        {
          lado = 1;
        }
        else
        {
          lado = 2;
        }
      }
      return lado;
      
}
void ISR_ENC()
{ 
  if(State == MapaZ)
  {
      int lado = getLado();
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        need_update = true;
      }
      if (lado == 1) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        if(posRel < 4)
        {
          posRelAnt = posRel;
          posRel++;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }
      if (lado == 2) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        else if(posRel > 1)
        {
          posRelAnt = posRel;
          posRel--;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }

      if(leerBoton())
      {
        switch(posRel)
        {
          case 1:
            
            break;
          case 2:
            maxX = actuatorPos.x;
            maxY = actuatorPos.y;
#ifdef DEBUG
            Serial.print(F("MAX X: "));
            Serial.println(maxX);
            Serial.print(F("MAX Y: "));
            Serial.println(maxY);
#endif
            beep();
            break;
          case 3:
            break;
          case 4:          
            mapaZ(puntosCal, maxX, maxY);
            break;
        }
        
      }

      if(leerStop())
      {
        changeState(Calibration);
        need_update = true;
        beep();
      }
      
  }
  
  
  if(State == Calibration)
  {
    int lado = getLado();
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        need_update = true;
      }
      if (lado == 1) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        if(posRel < 9)
        {
          posRelAnt = posRel;
          posRel++;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }
      if (lado == 2) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        else if(posRel > 1)
        {
          posRelAnt = posRel;
          posRel--;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }

      if(leerBoton())
      {
        switch(posRel)
        {
          case 1:
            if(ejeCal  < 3)
            {
              ejeCal ++;
            }
            else
            {
              ejeCal = 1;
            }
            need_update = true;
            break;
          case 2:
            if(multCal  < 3)
            {
              multCal ++;
            }
            else
            {
              multCal = 1;
            }
            need_update = true;
            break;
          case 3:
            proyCero.x = actuatorPos.x;
            proyCero.y = actuatorPos.y;
            proyCero.z = actuatorPos.z;
            need_update = true;
            break;
          case 6:          
            if(!usilloPrendido)
            {
              ctrlUsillo(3);
            }
            else
            {
              ctrlUsillo(5);
            }
            break;
          case 7:
            setZeroZ();
            break;
          case 8:
            changeState(MapaZ);
            beep();
            need_update = true;
            break;
          case 9:
            goHome();
            break;
        }
        beep(); 
      }
      
      if(leerStop())
      {
        changeState(Home);
        beep();
      }
  }
 
  if(State == SiNo)
  {
#ifdef DEBUG
      //Serial.println(posRel);
#endif
      int lado = getLado();
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
      }
      if (lado == 1) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        if(posRel <= 2)
        {
          posRelAnt = posRel;
          posRel++;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }
      if (lado == 2) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        else if(posRel > 1)
        {
          posRelAnt = posRel;
          posRel--;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }

      if(leerBoton())
      {
        beep();
        if(posRel == 2)
        {
          changeState(StateAnt);
        }
        if(posRel == 1)
        {
          //rebootFM(53, dir);
          changeState(Printing);
          fileLines = getLines(currentFile);
#ifdef DEBUG
          Serial.println(fileLines);
#endif         
          printFile = sdMain.open(currentFile);
          
        }
      }
  }
  if(State == SelectFile)
  {
      int lado = getLado();
      if(pos == -1)
      {
        posAnt = pos;
        pos = 0;
        posRel = (pos%4) + 1;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      if (lado == 1) 
      {
        if(pos == -1)
        {
          posAnt = pos;
          pos = 0;
          posRel = (pos%4) + 1;
          if(posRel != posRelAnt)
          {
            need_update = true;
          }
          else
          {
            need_update = false;
          }
          return;
        }
        else if(pos < fileNum()-1)
        {
          posAnt = pos;
          pos++;
          posRel = (pos%4) + 1;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      if (lado == 2) 
      {
        if(pos == -1)
        {
          posAnt = pos;
          pos = 0;
          posRel = (pos%4) + 1;
          if(posRel != posRelAnt)
          {
            need_update = true;
          }
          else
          {
            need_update = false;
          }
          return;
        }
        else if(pos > 0)
        {
          posAnt = pos;
          pos--;
          posRel = (pos%4) + 1;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }

      if(leerBoton())
      {
        currentFile = dir + files[pos];
#ifdef DEBUG
        Serial.println(currentFile);
#endif
        beep();
        need_update = true;
        changeState(SiNo);
      }
#ifdef DEBUG
      //Serial.println(leerBoton());
      //Serial.println(leerStop());
#endif
      if(leerStop())
      {
        changeState(Home);
        need_update = true;
        beep();
      }
    
      
  }

  if(State == Home)
  {
    int lado = getLado();
    if(posRel == -10)
    {
      posRelAnt = posRel;
      posRel = 1;
      if(posRel != posRelAnt)
      {
        need_update = true;
      }
      else
      {
        need_update = false;
      }
      return;
    }
    if (lado == 1) 
    {
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      else if(posRel < 3)
      {
        posRelAnt = posRel;
        posRel++;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
      }
      return;
    }
    if (lado == 2) 
    {
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      else if(posRel > 1)
      {
        posRelAnt = posRel;
        posRel--;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
      }
      return;
    }
    if(leerBoton())
    {
      if(posRel == 1)
      {
        changeState(SelectFile);
        need_update = true;
        beep();
      }
      if(posRel == 2)
      {
        changeState(Calibration);
        need_update = true;
        beep();
      }
      if(posRel == 3)
      {
        beep();
        resetFunc();
      }
      
    }
  }
  if(State == Printing)
  {
    if(leerStop())
    {
      beep();
      printProgress = 0;
      oldProgress = 0;
      ctrlUsillo(5);
      changeState(Home);
      need_update = true;
    }
  }
}

void beep()
{
  digitalWrite(44, HIGH);
#ifdef DEBUG
  Serial.println(F("Beep"));
#endif
  delay(100);
  digitalWrite(44, LOW);
}


void sdTick()
{
  if(State != Printing)
  {
    if(printFile)
    {
      printFile.close();
    }
  }
}



bool leerStop()
{
  stopBtn = digitalRead(46);
  bool res;
  if(stopBtn == 0 && stopBtnAnt == 1)
    {
      stopBtnAnt = stopBtn;
      res = true;
    }
    else
    {
      stopBtnAnt = stopBtn;
      res = false;
    }
    return res;
}

int leerBoton()
{
    boton = digitalRead(A8);
    bool res;    
    if (boton == 0 && botonAnt == 1) 
    {
      botonAnt = boton;
#ifdef DEBUG
      //Serial.println(F("ACA"));
#endif
      res = true;
      
    }
    else
    {
      botonAnt = boton;
      res = false;
    }
    return res;
}

void DPTick()
{
   
   if(State == Init)
   {
      DPinit();
   }

   if(State == MapaZ)
   {
      if(need_update)
      {
        DPMapaZ(posRel, puntosCal);
        need_update = false;
      }
   }
   if(State == SiNo)
   {
      if(need_update)
      {
        DPsiNo(true,"Desea Imprimir?");
        need_update = false;
        if(posRel == 1)
        {
          DPsiNo(true,"Desea Imprimir?");
          posRelAnt = posRel;
        }
        if(posRel == 2)
        {
          DPsiNo(false,"Desea Imprimir?");
          posRelAnt = posRel;
        }
      }
      
        
      
   }
   else if(State == Home)
   {
      if(need_update)
      {      
        posRelAnt = posRel;
        DPloadData(MENUHOME, 1, "Seleccionar Archivos", posRel, HOME);
        DPloadData(MENUHOME, 2, "Calibracion", posRel, HOME);
        DPloadData(MENUHOME, 3, "Reiniciar", posRel, HOME);
        DPhome();
        need_update = false;
      }
   }
   else if(State == SelectFile)
   {
      if(need_update)
      {
        currentMenu = (pos/4) + 4;
        posRelAnt = posRel;
        updateFilesDP(currentMenu, posRel);
        DPDrawMenu(currentMenu);
        need_update = false;
      }
      

   }
   else if(State == Printing)
   {
      if(need_update)
      {
        DPimprimiendo(printProgress, feedRate, useMM, absMode, spindleSpeed);
        need_update = false;
      }
      float res = printProgress - oldProgress;
      if(res > 0.1)
      {
        DPimprimiendo(printProgress, feedRate, useMM, absMode, spindleSpeed);
        oldProgress = printProgress;
      }
      
   }
   else if(State == Calibration)
   {
      if(need_update)
      {
        DPcalibration(ejeCal, proyCero.x,proyCero.y, proyCero.z, multCal, useMM, posRel);
        need_update = false;
      }
   }
   
   
}

void updateFilesDP(int Menu, int hili)
{
   
   for(int j = 0; j<fileNum(); j++)
     {       
       
       int Rest = j%4;
       DPloadData((j/4)+4, Rest + 1, files[j], hili, 2);
       
     }

     
   
   
//   for(int j = 0; j<fileNum() && j < 4; j++)
//   {
//     int DPpos = j+1;
//     DPloadData(Menu, DPpos, files[j], hili, 2);
//   }
   
}

void loadFilesToDP()
{
   
     for(int j = 0; j<fileNum(); j++)
     {       
       int Rest = j%4;
       DPloadData((j/4)+4, Rest + 1, files[j], -1, FILES);
     }
     
   
}

void finalCarreraX()
{
  if(digitalRead(FINX1) == LOW)
  {
      ejeX.startBrake();
#ifdef DEBUG
    //Serial.println(F("FINX1"));
#endif
    ejeX.move(800);
    actuatorPos.x = 0;
    esFinX1 = true;
  }
  else
  {
    esFinX1 = false;
  }
  if(digitalRead(FINX2) == LOW)
  {
      ejeX.startBrake();
#ifdef DEBUG
    //Serial.println(F("FINX2"));
#endif
    ejeX.move(-800);
    actuatorPos.x = 150;
    esFinX2 = true;
  }
  else
  {
    esFinX2 = false;
  }
  return;
    
}
void finalCarreraY()
{
   
    if(digitalRead(FINY1) == LOW)
    {
      ejeY.startBrake();
#ifdef DEBUG
      //Serial.println(F("FINY1"));
#endif
      ejeY.move(800);
      actuatorPos.y = 0;
      esFinY1 = true;
    }
    else
    {
      esFinY1 = false;
    }
    if(digitalRead(FINY2) == LOW)
    {
        ejeY.startBrake();
#ifdef DEBUG
      //Serial.println(F("FINY2"));
#endif
      ejeY.move(-800);
      actuatorPos.y = 139;
      esFinY2 = true;
    }
    else
    {
      esFinY2 = false;
    }
    return;
}
void finalCarreraZ()
{
    if(digitalRead(FINZ) == LOW)
    {
      ejeZ.startBrake();
#ifdef DEBUG
      //Serial.println(F("FINZ"));
#endif
      actuatorPos.z = -9999999;
      esFinZ = true;
    }
    else 
    {
      esFinZ = false;
    }
    return;
}

/**********************
 * void setup() - Initialisations
 ***********************/
void setup() {
  //  Setup
#ifdef DEBUG
  Serial.begin( 9600 ); 
#endif
 //INICIO DP
 DPBegin(12); //Funcion que inicia el DP (Display)
 changeState(Init);
 DPinit();

 proyCero.x = 0;
 proyCero.y = 0;
 proyCero.z = 0;

 ejeCal = 1;
 multCal = 1;

 proyCero.x = 0;
 proyCero.y = 0;
 proyCero.z = 0;

 actuatorPos.z = 0;

  
 //INICIOSD
 FMBegin(53, dir); // Inicio la SD, ver Archivo FileManager.hpp
 getFiles(files); // Inicio leo los archivos y le paso un puntero de mi array, ver Archivo FileManager.hpp
 int nFiles2 = fileNum(); // Obtengo la cantidad de archivos leidos, ver Archivo FileManager.hpp

 int delayReboot = 10;

 if (!sdMain.begin(53)) 
 { //make sure sd card was found
   while (true)
   {
     String tit = "Reseteando en " + (String)delayReboot;
     DPerror("Error SD", tit.c_str());
     delayReboot--;
     delay(1000);
     if(delayReboot == 0)
     {
        resetFunc();
     }
   }
 }

 //SETEO LA CANTIDAD DE MENUES SEGUN CANT DE ARCHIVOS
 int cantM = nFiles2/4;
 bool hayResto = nFiles2%4;
 if(hayResto)
 {
    cantM++;
 }

 cantM += 4;


 if(digitalRead(FINX1) == LOW) esFinX1 = true;
 if(digitalRead(FINX2) == LOW) esFinX2 = true;
 if(digitalRead(FINY1) == LOW) esFinY1 = true;
 if(digitalRead(FINY2) == LOW) esFinY2 = true;
 if(digitalRead(FINZ) == LOW) esFinZ = true;

 
 DPSetMenuQ(cantM);

 loadFilesToDP();
  //ENCODER
  pinMode(btnENC, INPUT_PULLUP);
  pinMode(btnStop, INPUT_PULLUP);
  r.begin(true);


  //BEEPER

  pinMode(beeper, OUTPUT);
  digitalWrite(beeper, LOW);


  // Configuracion Motores
  //EJE Z
//  ejeZ.setMaxSpeed(VELMOTPAP); 
//  ejeZ.setAcceleration(8000);
//  ejeZ.setSpeed(VELMOTPAP);
//  ejeZ.setEnablePin(EN_Z);
//  ejeZ.setPinsInverted(false,false,true);  
//  ejeZ.enableOutputs();
//  ejeZ.setCurrentPosition(0);

    ejeZ.begin(VELMOTPAP, 1);
  //EJE Y
//  ejeY.setMaxSpeed(VELMOTPAP); 
//  ejeY.setAcceleration(8000);
//  ejeY.setSpeed(VELMOTPAP);
//  ejeY.setEnablePin(EN_Y);
//  ejeY.setPinsInverted(false,false,true); 
//  ejeY.enableOutputs();
//  ejeY.setCurrentPosition(0);
    ejeY.begin(VELMOTPAP, 1);
  //EJE X
//  ejeX.setMaxSpeed(VELMOTPAP); 
//  ejeX.setAcceleration(8000);
//  ejeX.setSpeed(VELMOTPAP);
//  ejeX.setEnablePin(EN_X);
//  ejeX.setPinsInverted(false,false,true); 
//  ejeX.enableOutputs();
//  ejeX.setCurrentPosition(0);
    ejeX.begin(VELMOTPAP, 1);
  

  //USILLO

  pinMode(PWM_USILLO, OUTPUT);
  digitalWrite(PWM_USILLO,LOW);
  usilloPrendido = true;

  //CONFIGURACION FINES DE CARRERA
  //EJE X
  pinMode(FINX1, INPUT_PULLUP);
  pinMode(FINX2, INPUT_PULLUP);
  attachPCINT(digitalPinToPinChangeInterrupt(FINX1),finalCarreraX,CHANGE);
  attachPCINT(digitalPinToPinChangeInterrupt(FINX2),finalCarreraX,CHANGE);
  //EJE Y
  pinMode(FINY1, INPUT_PULLUP);
  pinMode(FINY2, INPUT_PULLUP);
  attachPCINT(digitalPinToPinChangeInterrupt(FINY1),finalCarreraY,CHANGE);
  attachPCINT(digitalPinToPinChangeInterrupt(FINY2),finalCarreraY,CHANGE);
  //EJE Z
  pinMode(FINZ, INPUT_PULLUP);
  attachPCINT(digitalPinToPinChangeInterrupt(FINZ),finalCarreraZ,CHANGE);

  //CONFIGURACION CONTINUIDAD

  pinMode(ZCAL_TEST, INPUT_PULLUP);

  //CONFIGURACION BOTONES

  buttonMas.registerCallbacks(buttonMas_pressedCallback,NULL,NULL,NULL);
  buttonMenos.registerCallbacks(buttonMenos_pressedCallback, NULL,NULL,NULL);

  buttonMas.setup(BUTTON_MAS, BUTTON_DEBOUNCE_DELAY, InputDebounce::PIM_INT_PULL_UP_RES);
  buttonMenos.setup(BUTTON_MENOS, BUTTON_DEBOUNCE_DELAY, InputDebounce::PIM_INT_PULL_UP_RES);


  //  Notifications!!!
#ifdef DEBUG
  Serial.println(F("Mini CNC Plotter alive and kicking!"));
  Serial.print(F("X range is from ")); 
  Serial.print(Xmin); 
  Serial.print(F(" to ")); 
  Serial.print(Xmax); 
  Serial.println(F(" mm.")); 
  Serial.print(F("Y range is from ")); 
  Serial.print(Ymin); 
  Serial.print(F(" to ")); 
  Serial.print(Ymax); 
  Serial.println(F(" mm.")); 
#endif

  delay(2000);
  DPinit2();
  delay(2000);
  goHome();
  actuatorPosRel.x = 0;
  actuatorPosRel.y = 0;
  actuatorPosRel.z = 0;
  changeState(Home);

}

void flushBuffer(char * line)
{
  for(int i = 0; i < LINE_BUFFER_LENGTH; i++)
  {
    line[i] = NULL;
  }
}

void ctrlUsillo(int acc)
{
  switch(acc)
  {
    case 3:
#ifdef DEBUG
      Serial.println(F("Prendi"));
#endif
      digitalWrite(PWM_USILLO,HIGH);
      usilloPrendido = true;
      break;
    case 4:
#ifdef DEBUG
      Serial.println(F("Prendi"));
#endif
      digitalWrite(PWM_USILLO, HIGH);
      usilloPrendido = true;
      break;
    case 5:
#ifdef DEBUG
      Serial.println(F("APAGUE"));
#endif
      digitalWrite(PWM_USILLO, LOW);
      usilloPrendido = false;
      break;
  }
}

void setZeroZ()
{
  if(actuatorPos.x != 0 && digitalRead(ZCAL_TEST) == HIGH)
  {
    drawLine(0, actuatorPos.y, actuatorPos.z);
    updateActuatorPos(0, actuatorPos.y, actuatorPos.z, false);
  }

  if(actuatorPos.y != 0 && digitalRead(ZCAL_TEST) == HIGH)
  {
    drawLine(actuatorPos.x, 0, actuatorPos.z);
    updateActuatorPos(actuatorPos.x, 0, actuatorPos.z, false);
  }
  
  if(digitalRead(ZCAL_TEST) == LOW)
  {
    actuatorPos.z = 0;
  }
  while(actuatorPos.z != 0)
  {
    float rev = -0.1 * REV_PER_MM_Z;
    long steps = rev * STEPS_PER_REV_Z;
    
    ejeZ.move(steps);
    
    if(digitalRead(ZCAL_TEST) == LOW)
    {
      actuatorPos.z = 0;
    } 
  }

  drawLine(actuatorPos.x,actuatorPos.y, 5);
  updateActuatorPos(actuatorPos.x,actuatorPos.y, 5, false);
}


/*********************************
 * Draw a line from (x0;y0) to (x1;y1). 
 * Bresenham algo from https://www.marginallyclever.com/blog/2013/08/how-to-build-an-2-axis-arduino-cnc-gcode-interpreter/
 * int (x1;y1) : Starting coordinates
 * int (x2;y2) : Ending coordinates
 **********************************/
void drawLine(float x1, float y1, float z1) 
{

  if(absMode && useMM)
  {
#ifdef DEBUG
    //Serial.println(F("-----------INICIO MOVIMIENTO--------------"));
#endif
    struct point relPoint;
    struct point StepsMM;
    struct point RelMM;
    int zstep = 0;

#ifdef DEBUG

//    Serial.print(F("X: "));
//    Serial.println(x1);
//    Serial.print(F("Y: "));
//    Serial.println(y1);
//    Serial.print(F("Z: "));
//    Serial.println(z1);
//
//    Serial.print(F("ActuatorPos X: "));
//    Serial.println(actuatorPos.x);
//    Serial.print(F("ActuatorPos Y: "));
//    Serial.println(actuatorPos.y);
//    Serial.print(F("ActuatorPos Z: "));
//    Serial.println(actuatorPos.z);
//
//    Serial.print(F("ActuatorPosRel X: "));
//    Serial.println(actuatorPosRel.x);
//    Serial.print(F("ActuatorPosRel Y: "));
//    Serial.println(actuatorPosRel.y);
//    Serial.print(F("ActuatorPosRel Z: "));
//    Serial.println(actuatorPosRel.z);

#endif
    
    relPoint.x = x1-actuatorPos.x;
    relPoint.y = y1-actuatorPos.y;
    relPoint.z = z1-actuatorPos.z;


#ifdef DEBUG
//    Serial.print(F("RelPoint X: "));
//    Serial.println(relPoint.x);
//    Serial.print(F("RelPoint Y: "));
//    Serial.println(relPoint.y);
//    Serial.print(F("RelPoint Z: "));
//    Serial.println(relPoint.z);
#endif

   
    
      RelMM.z = relPoint.z * REV_PER_MM_Z;
      StepsMM.z = RelMM.z * STEPS_PER_REV_Z;

#ifdef DEBUG
      Serial.print(F("Pasos Z: "));
      Serial.println(StepsMM.z);
#endif
      
      
  
      RelMM.x = relPoint.x * REV_PER_MM_X;
      StepsMM.x = RelMM.x * STEPS_PER_REV_X;  
#ifdef DEBUG
      Serial.print(F("Pasos X: "));
      Serial.println(StepsMM.x); 
#endif

     
      RelMM.y = relPoint.y * REV_PER_MM_Y;
      StepsMM.y = RelMM.y * STEPS_PER_REV_Y;
#ifdef DEBUG
      Serial.print(F("Pasos Y: "));
      Serial.println(StepsMM.y);
#endif


      float divX = maxXDef / cantPuntDef;
      float divY = maxYDef / cantPuntDef;

      float valoX[cantPuntDef+1];
      float valoY[cantPuntDef+1];

      int puntoX;
      int puntoXAnt;

      int puntoY;
      int puntoYAnt;


      float offsetX;
      float offsetY;
      float offsetXAnt;
      float offsetYAnt;

      float promX;
      float promY;


      for(int x = 0; x <= cantPuntDef; x++)
      {
        valoX[x] = divX*x;
      }

      for(int y = 0; y <= cantPuntDef; y++)
      {
        valoX[y] = divY*y;
      }

      for(int ix = 0; ix < sizeof(valoX); ix++)
      {
        if(x1 < valoX[ix])
        {
          puntoX = ix;
          puntoXAnt = ix-1;
          break;
        }
      }

      for(int iy = 0; iy < sizeof(valoY); iy++)
      {
        if(y1 < valoY[iy])
        {
          puntoY = iy;
          puntoYAnt = iy-1;
          break;
        }
      }

      

      //offsetX = (x1*table[puntoY][puntoX])/puntoX;
      //offsetY = (y1*table[puntoY][puntoX])/puntoY;
      //offsetXAnt = (x1*table[puntoYAnt][puntoXAnt])/puntoXAnt;
      //offsetYAnt = (y1*table[puntoYAnt][puntoXAnt])/puntoYAnt;

      //promX = (offsetX + offsetXAnt)/2;
      //promY = (offsetY + offsetYAnt)/2;


      //offsetZ = (promX + promY)/2; 
      

      runEje(StepsMM.x, StepsMM.y, StepsMM.z);
      
      
      
#ifdef DEBUG
    //Serial.println(F("-------------TERMINO MOVIMIENTO--------------"));
#endif
  }

}




void runEje(float stepsX, float stepsY, float stepsZ)
{
  struct point StepsMM;
  struct point RelMM;
  
  
  if(stepsZ != 0)
  {
    ejeZ.move(stepsZ);
  }
  if(stepsX != 0)
  {
    ejeX.startMove(stepsX);
  }
  if(stepsY != 0)
  {
    ejeY.startMove(stepsY);
  }

  while(ejeX.getStepsRemaining() != 0 || ejeY.getStepsRemaining() != 0)
  {
    if(ejeX.getStepsRemaining() != 0)
    {
      ejeX.nextAction();
    }
    if(ejeY.getStepsRemaining() != 0)
    {
      ejeY.nextAction();
    }
  }
}

/**********************
 * void loop() - Main loop
 ***********************/
void loop() 
{
  
  delay(200);
  char line[ LINE_BUFFER_LENGTH ];
  char c;
  int lineIndex;
  bool lineIsComment, lineSemiColon, lineWasCommented;

  int delayReboot = 10;
 

  lineIndex = 0;
  lineSemiColon = false;
  lineIsComment = false;
  actuatorPos.z = 0;
  unsigned long now = millis();
  
  
  while (1) 
  {
    ISR_ENC();
    DPTick();
    now = millis();
    buttonMas.process(now); // callbacks called in context of this function
    buttonMenos.process(now);  


#ifdef DEBUG
    //Serial.println(analogRead(A1));
#endif

    if(State==Calibration)
    {
     
    }
    if(State == Printing)
    {
      currentLine = 0;
      oldProgress = 0;
      need_update = true;
      DPTick(); 
           
      if(!printFile)
      {
         DPerror("2ERROR AL", "ABRIR EL ARCHIVO");

         while(1)
         {
            if(leerStop())
            {
              changeState(Home);
              break;
            }
         }
        
        
      }
      
      while (printFile.available()) 
      {
          ISR_ENC();
          DPTick();
          now = millis();
          buttonMas.process(now); // callbacks called in context of this function
          buttonMenos.process(now);
          if(State != Printing) break;
    
          if (GCode.AddCharToLine(printFile.read()))
          {
            char * line = GCode.line;
#ifdef DEBUG
            Serial.println(F(line));
#endif
            GCode.ParseLine();
            currentLine++;
            printProgress = ((float)currentLine / (float)fileLines); 
            if(printProgress == 1)
            {
              oldProgress = 0;
              printProgress = 0;
              changeState(Home);
            }
            processIncomingLine();
          }
      }
    }
    
  }
}

void arcToSegments(bool isCW)
{
          float toX;
          float toY;
          float i;
          float j;
          float tempFeedRate;
          float mmPs;
          float revPmm;
          float stepPmm;
          int tempSpindleSpeed;
          //https://marlinfw.org/docs/gcode/G002-G003.html
          float maxSegmentLen = 1;
          float maxSegmentAngle = ((float)5 / (float)180) * PI;        

          if(GCode.HasWord('X'))
          {
            if(absMode)
            {
              toX = GCode.GetWordValue('X');
            }
            else
            {
              toX = actuatorPos.x + GCode.GetWordValue('X');
            }
          }
          else
          {
#ifdef DEBUG
            Serial.println(F("Error G02/G03 No hay X"));
#endif
            return;
          }
          if(GCode.HasWord('Y'))
          {
            if(absMode)
            {
              toY = GCode.GetWordValue('Y');
            }
            else
            {
              toY = actuatorPos.x + GCode.GetWordValue('Y');
            }
          }
          else
          {
#ifdef DEBUG            
            Serial.println(F("Error G02/G03 No hay Y"));
#endif
            return;
          }
          if(GCode.HasWord('I'))
          {
            i = GCode.GetWordValue('I');
          }
          else
          {
#ifdef DEBUG
            Serial.println(F("Error G02/G03 No hay I"));
#endif
            return;
          }
          if(GCode.HasWord('J'))
          {
            j = GCode.GetWordValue('J');
          }
          else
          {
#ifdef DEBUG
            Serial.println(F("Error G02/G03 No hay J"));
#endif
            return;
          }

          if(GCode.HasWord('F'))
          {
            
//            tempFeedRate = GCode.GetWordValue('F');
//            if(tempFeedRate != feedRate)
//            {
//              mmPs = feedRate*0.016667;
//              revPmm = mmPs * REV_PER_MM_X;
//              stepPmm = revPmm * STEPS_PER_REV_X;
//              VELMOTPAP = stepPmm; 
//              need_update = true;
//            }
            
          }
          float fromX = actuatorPos.x;
          float fromY = actuatorPos.y;


          float rad = sqrt(i * i + j * j);

          float cX = fromX + i;
          float cY = fromY + j;

          float startAngle = atan2(fromY - cY, fromX - cX);
          float endAngle = atan2(toY - cY, toX - cX);

          float aX = fromX - cX;
          float aY = fromY - cY;
          float bX = toX - cX;
          float bY = toY - cY;
          // https://stackoverflow.com/questions/40286650/how-to-get-the-anti-clockwise-angle-between-two-2d-vectors
          float ccwAngle = atan2(aX * bY - aY * bX, aX * bX + aY * bY);
          if (ccwAngle < 0) ccwAngle += PI * 2;

          float totalDeltaAngle = ccwAngle;
          if (isCW) totalDeltaAngle = totalDeltaAngle - PI * 2;


          float bowLen = abs(totalDeltaAngle) * rad;
          // how many segments needs the bow to be divided into

          float requiredSegmentsByLengthLimit = ceil(
            bowLen / maxSegmentLen
          );
          float requiredSegmentsByAngleLimit = ceil(
            abs(totalDeltaAngle) / maxSegmentAngle
          );

          float requiredSegments = max(
            requiredSegmentsByLengthLimit,
            requiredSegmentsByAngleLimit
          );



          

          float deltaAngle = totalDeltaAngle / requiredSegments;

          float deltaSegmentLen = maxSegmentAngle;
          if (deltaAngle > maxSegmentAngle) {
            // angle between to big for set limits, make smaller segments
            deltaAngle = maxSegmentAngle;
            deltaSegmentLen = deltaAngle * rad;
            requiredSegments = ceil(bowLen / deltaSegmentLen);
          }

          String output = "";
          if (requiredSegments > 1) {
            for (int cont = 1; cont < requiredSegments; cont++) {
              float angle = startAngle + cont * deltaAngle;
              float segX;
              float segY;
              if(absMode)
              {
                segX = (cX + cos(angle) * rad);
                segY = (cY + sin(angle) * rad);
              }
              else
              {
                segX = (cos(angle) * rad);
                segY = (sin(angle) * rad);
              }
              
              drawLine(segX, segY, actuatorPos.z);
              updateActuatorPos(segX, segY, actuatorPos.z, true);
            }
          }

}

void updateActuatorPos(float x, float y, float z, bool isRel)
{
  if(isRel)
  {
    actuatorPosRel.x = x;
    actuatorPosRel.y = y;
    actuatorPos.z = z ;
    actuatorPos.x = x + proyCero.x;
    actuatorPos.y = y + proyCero.y;
  }
  else
  {
    actuatorPos.x = x;
    actuatorPos.y = y;
    actuatorPos.z = z;
    
  }

  return;

  
  
  
}

void processIncomingLine() 
{
  
    struct point newPos;
    newPos.x = 0;
    newPos.y = 0;
    newPos.z = 0;
    
    float tempFeedRate;
    float mmPs;
    float revPmm;
    float stepPmm;
    int tempSpindleSpeed;
    
    if(GCode.HasWord('G'))
    {
      switch((int)GCode.GetWordValue('G'))
      {
        case 0:
        case 1:
          if(GCode.HasWord('F'))
          {
//            tempFeedRate = GCode.GetWordValue('F');
//            if(tempFeedRate != feedRate)
//            {
//              mmPs = feedRate*0.016667;
//              revPmm = mmPs * REV_PER_MM_X;
//              stepPmm = revPmm * STEPS_PER_REV_X;
//              VELMOTPAP = stepPmm; 
//              need_update = true;
//            }
          }
          if(GCode.HasWord('X'))
          {
            newPos.x = GCode.GetWordValue('X');
          }
          else
          {
            newPos.x = actuatorPos.x;
          }
          if(GCode.HasWord('Y'))
          {
            newPos.y = GCode.GetWordValue('Y');
          }
          else
          {
            newPos.y = actuatorPos.y;
          }
          if(GCode.HasWord('Z'))
          {
            newPos.z = GCode.GetWordValue('Z');
          }
          else
          {
            newPos.z = actuatorPos.z;
          }
          
          drawLine(newPos.x, newPos.y, newPos.z);
          updateActuatorPos(newPos.x, newPos.y, newPos.z, true);
          break;
        case 2:
          if(GCode.HasWord('F'))
          {
//            tempFeedRate = GCode.GetWordValue('F');
//            if(tempFeedRate != feedRate)
//            {
//              mmPs = feedRate*0.016667;
//              revPmm = mmPs * REV_PER_MM_X;
//              stepPmm = revPmm * STEPS_PER_REV_X;
//              VELMOTPAP = stepPmm; 
//              need_update = true;
//            }
          }
          arcToSegments(true);
          break;
        case 3:
          if(GCode.HasWord('F'))
          {
//            tempFeedRate = GCode.GetWordValue('F');
//            if(tempFeedRate != feedRate)
//            {
//              mmPs = feedRate*0.016667;
//              revPmm = mmPs * REV_PER_MM_X;
//              stepPmm = revPmm * STEPS_PER_REV_X;
//              VELMOTPAP = stepPmm; 
//              need_update = true;
//            }         
          }
          arcToSegments(false);
          break;
        case 20:
          useMM = false;
          need_update = true;
          break;
        case 21:
          useMM = true;
          need_update = true;
          break;
        case 28:
          goHome();
          break;
        case 90:
          absMode = true;
          need_update = true;
          break;
        case 91:
          absMode = false;
          need_update = true;
          break;
    
        default:
          break;
    }
      
      
      
    }
    else if(GCode.HasWord('M'))
    {
      switch((int)GCode.GetWordValue('M'))
      {
            case 2:
#ifdef DEBUG
              Serial.println(F("Termino Programa"));
#endif
              break;
            case 3:
              ctrlUsillo(3);
              break;
            case 4:
              ctrlUsillo(4);
              break;
            case 5: 
              ctrlUsillo(5);
              break;
            default:
#ifdef DEBUG
              Serial.println(F("Command not recognized : M"));
#endif
              break;
      }
    }
    else if(GCode.HasWord('F'))
    {
      tempFeedRate = GCode.GetWordValue('F');
      if(tempFeedRate != feedRate)
      {
//        mmPs = feedRate*0.016667;
//        revPmm = mmPs * REV_PER_MM_X;
//        stepPmm = revPmm * STEPS_PER_REV_X;
//        VELMOTPAP = stepPmm; 
//        need_update = true;
      }
    }
    else if(GCode.HasWord('S'))
    {
      tempSpindleSpeed = (int)GCode.GetWordValue('S');
      if(tempSpindleSpeed != spindleSpeed)
      {
        spindleSpeed = tempSpindleSpeed;
        need_update = true;
      }
      
    }


}


void mapaZ(int cantPunt, float maxX, float maxY)
{
  float divX = maxX / cantPunt;
  float divY = maxY / cantPunt;

  float zSteps = actuatorPos.z;

//  table = new float*[cantPunt+1];
//
//  for(int i = 0; i < cantPunt+1; i++)
//  {
//    table[i] = new float[cantPunt+1];
//  }

  bool toco = false;

  struct point StepsMM;
  struct point RelMM;

  int oldSpeed = VELMOTPAP;

  VELMOTPAP = 6000;

  drawLine(0,0,1);
  updateActuatorPos(0,0,1,false);

  zSteps = actuatorPos.z;

  for(int stepsY = 0; stepsY <= cantPunt; stepsY++)
  {
    zSteps = actuatorPos.z;
    toco = false;

    VELMOTPAP = 6000;
    drawLine(actuatorPos.x, actuatorPos.y, 1);
    updateActuatorPos(actuatorPos.x, actuatorPos.y, 1, false);

    
    for(int stepsX = 0; stepsX <= cantPunt; stepsX++)
    {
      
#ifdef DEBUG
      //Serial.print(F("Z1: "));
      //Serial.println(actuatorPos.z);
#endif
      zSteps = actuatorPos.z;
      toco = false;
      
      drawLine(divX*stepsX, actuatorPos.y, actuatorPos.z);
      updateActuatorPos(divX*stepsX, actuatorPos.y, actuatorPos.z, false);
        
 
      if(digitalRead(ZCAL_TEST) == LOW)
      {
        //table[stepsY][stepsX] = actuatorPos.z;
        toco = true;
#ifdef DEBUG
        //Serial.println(F("TOCO: SI"));
#endif
        beep();
      }
      else
      {
#ifdef DEBUG
        //Serial.println(F("TOCO: NO"));
#endif
      }

      VELMOTPAP = 1000;
      while(!toco)
      {
        
        drawLine(actuatorPos.x, actuatorPos.y, zSteps);
        updateActuatorPos(actuatorPos.x,actuatorPos.y,zSteps, false);
        
        if(digitalRead(ZCAL_TEST) == LOW)
        {
          //table[stepsY][stepsX] = actuatorPos.z;
          toco = true;
#ifdef DEBUG          
          //Serial.println(F("TOCO: SI"));
#endif
          beep();
          drawLine(actuatorPos.x, actuatorPos.y, 1);
          updateActuatorPos(actuatorPos.x, actuatorPos.y, 1,false);
        }
        else
        {
          zSteps-=0.1;
#ifdef DEBUG            
          //Serial.println(F("TOCO: NO"));
#endif
        }  
      }
     
    }

    drawLine(0,divY*(stepsY+1),1);
    updateActuatorPos(0,divY*(stepsY+1),1, false);
    
  }

  

  drawLine(0,0,5);
  updateActuatorPos(0,0,5, false);
  VELMOTPAP = oldSpeed;

  for(int Y = 0; Y<=cantPunt; Y++)
  {
    for(int X = 0; X<=cantPunt; X++)
    {
#ifdef DEBUG      
      //Serial.print(table[Y][X]);
#endif
      if(X == cantPunt)
      {
#ifdef DEBUG
        Serial.println(F(""));
#endif
      }
      else
      {
#ifdef DEBUG
        Serial.print(F("          "));
#endif
      }     
    }
  }
  beep();

  cantPuntDef = cantPunt;
  maxXDef = maxX;
  maxYDef = maxY;

}
