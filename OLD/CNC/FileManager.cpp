#include "FileManager.hpp"
#include "Pantalla.hpp"



String DIR;
int nFiles;
int delayReboot = 10;

SdFat sd;

void(* resetSoftware)(void) = 0;

void FMBegin(int pin, String dir)
{
 if (!sd.begin(pin)) { //make sure sd card was found
   while (true)
   {
     String tit = "Reseteando en " + (String)delayReboot;
     DPerror("Error SD", tit.c_str());
     delayReboot--;
     delay(1000);
     if(delayReboot == 0)
     {
        resetSoftware();
     }
   }
 }

  DIR = dir;
  return;
}

int fileNum()
{
  return nFiles;
}

void rebootFM(int pin, String dir)
{
  if (!sd.begin(pin)) { //make sure sd card was found
   while (true)
   {
     String tit = "Reseteando en " + (String)delayReboot;
     DPerror("Error SD", tit.c_str());
     delayReboot--;
     delay(1000);
     if(delayReboot == 0)
     {
        resetSoftware();
     }
   }
}
}

int getLines(String file)
{
  int num = 0;
  File arch = sd.open(file);
  char line[256];
  char c;
  int lineIndex = 0;
  if(!arch) Serial.println("ERROR AL ABRIR ARCHIVO");
  DPcargando();
  while(arch.available())
  {
      c = arch.read();
      if (c == '\n')
      {
        num++;
      } 
      if(c == '\r')
      {
        if(arch.read() == '\n')
        {
          num++;
        }
      }
      

      
  } 
  arch.close();
  return num;
}

void getFiles(String * files)
{
  
 nFiles = 0;
 File folder = sd.open(DIR.c_str());
 while(true){
   Serial.println("Aca");
   File entry = folder.openNextFile();
   if(!entry){
     folder.rewindDirectory();
     break;
   }else{
   nFiles++;
   }  
   entry.close();
 }
 
 for(int i = 0; i < nFiles; i++)
 {
   char fileName[60];
   File entry = folder.openNextFile();
   entry.getName(fileName,60);
   Serial.println(fileName);
   files[i] = String(fileName);
   entry.close();  
 }

 folder.close();
  
}
