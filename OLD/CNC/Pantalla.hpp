#ifndef _PANTALLA_H_
#define _PANTALLA_H_

#include <WString.h>                // include the String library
//extern U8G2_ST7920_128X64_F_SW_SPI *u8g2;

#define NONE -1
#define HOME 1
#define FILES 2
#define PRINTING 3
#define CALIBRATING 4

#define MENUHOME 0
#define MENUCALIBRATION 1
#define MENUPRINTING 2
#define MENUSINO 3
#define MENUMAPAZ 4

typedef String dataDef[8];

struct pant
{
  dataDef txt;
  int hili;
};
bool DPDrawMenu(int menu);
void DPBegin(int separacion);
int DPgetMenuQ();
void DPloadData(int menu, int pos, String str, int hili, int type);
void DPSetMenuQ(int menuQ);
void DPinit();
void DPinit2();
void DPcargando();
void DPMapaZ(int posRel, int puntosCal);
void DPhome();
void DPerror(char * errL1, char * errL2);
void DPimprimiendo(float progress, float feedRate, bool useMM, bool absMode, float vMot);
void progressBar(int x, int y, int w, int h, float value);
void DPsiNo(bool sel, char * title);
void DPselectFile(int pos);
void DPcalibration(int eje, float x0, float y0, float z0, int mult, bool isMM, int pos);
  //(U8G2_R0, /* clock=*/ 13, /* data=*/ 22, /* CS=*/ 12, /* reset=*/ 12); // Feather HUZZAH ESP8266, E=clock=14, RW=data=13, RS=CS  
    



#endif
