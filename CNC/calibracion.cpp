#include "calibracion.hpp"
#include "movimiento.hpp"
#include <AccelStepper.h>

int puntosCal = 2;
double maxX = 0;
double maxY = 0;
double maxXDef = 0;
double maxYDef = 0;
double offsetZ;
double offsetZAnt;
bool mapaZisDef = false;
int cantPuntDef;
int ejeCal = 1;
int multCal = 1;
double **table;
double multFEED = 1;



void mapaZ(int cantPunt, double maxX, double maxY)
{
  double divX = maxX / cantPunt;
  double divY = maxY / cantPunt;
  double feedRateA = feedRate;

  double OFFSETZ = 0;

  double zSteps = actuatorPos.z;

 table = new double*[cantPunt+1];

 for(int i = 0; i < cantPunt+1; i++)
 {
   table[i] = new double[cantPunt+1];
 }

  bool toco = false;

  struct point StepsMM;
  struct point RelMM;

  updateVel(1200);
  OFFSETZ = drawLine(0,0,1);
  updateActuatorPos(0,0,1,false);

  zSteps = actuatorPos.z;

  for(int stepsY = 0; stepsY <= cantPunt; stepsY++)
  {
    zSteps = actuatorPos.z;
    toco = false;

    OFFSETZ = drawLine(actuatorPos.x, actuatorPos.y, 1);
    updateActuatorPos(actuatorPos.x, actuatorPos.y, 1, false);

    
    for(int stepsX = 0; stepsX <= cantPunt; stepsX++)
    {
      
#ifdef DEBUG
      //Serial.print(F("Z1: "));
      //Serial.println(actuatorPos.z);
#endif
      zSteps = actuatorPos.z;
      toco = false;

      updateVel(1200);
      
      OFFSETZ = drawLine(divX*stepsX, actuatorPos.y, actuatorPos.z);
      updateActuatorPos(divX*stepsX, actuatorPos.y, actuatorPos.z, false);
        
 
      if(digitalRead(ZCAL_TEST) == LOW)
      {
        table[stepsY][stepsX] = actuatorPos.z;
        toco = true;
#ifdef DEBUG
        //Serial.println(F("TOCO: SI"));
#endif
        beep();
      }
      else
      {
#ifdef DEBUG
        //Serial.println(F("TOCO: NO"));
#endif
      }

      while(!toco)
      {
        
        updateVel(400);
        OFFSETZ = drawLine(actuatorPos.x, actuatorPos.y, zSteps);
        updateActuatorPos(actuatorPos.x,actuatorPos.y,zSteps, false);
        
        if(digitalRead(ZCAL_TEST) == LOW)
        {
          table[stepsY][stepsX] = actuatorPos.z;
          toco = true;
#ifdef DEBUG          
          //Serial.println(F("TOCO: SI"));
#endif
          beep();
          OFFSETZ = drawLine(actuatorPos.x, actuatorPos.y, 1);
          updateActuatorPos(actuatorPos.x, actuatorPos.y, 1,false);
        }
        else
        {
          zSteps-=0.05;
#ifdef DEBUG            
          //Serial.println(F("TOCO: NO"));
#endif
        }  
      }
     
    }
    updateVel(1200);

    if(stepsY < cantPunt)
    {
      OFFSETZ = drawLine(0,divY*(stepsY+1),1);
      updateActuatorPos(0,divY*(stepsY+1),1, false);
    }
    
  }

  
  updateVel(400);
  OFFSETZ = drawLine(actuatorPos.x,actuatorPos.y,5);
  updateActuatorPos(actuatorPos.x,actuatorPos.y,5, false);

  updateVel(1200);

  OFFSETZ = drawLine(0,0,actuatorPos.z);
  updateActuatorPos(0,0,actuatorPos.z, false);

  updateVel(feedRateA);

  for(int Y = 0; Y<=cantPunt; Y++)
  {
    for(int X = 0; X<=cantPunt; X++)
    {
#ifdef DEBUG      
      Serial.print(table[Y][X]);
#endif
      if(X == cantPunt)
      {
#ifdef DEBUG
        Serial.println(F(""));
#endif
      }
      else
      {
#ifdef DEBUG
        Serial.print(F("          "));
#endif
      }     
    }
  }
  beep();

  cantPuntDef = cantPunt;
  maxXDef = maxX;
  maxYDef = maxY;


}


void setZeroZ()
{
  double OFFSETZ = 0;

  long pasos[3];
  if(actuatorPos.x != 0 && digitalRead(ZCAL_TEST) == HIGH)
  {
    OFFSETZ = drawLine(0, actuatorPos.y, actuatorPos.z);
    updateActuatorPos(0, actuatorPos.y, actuatorPos.z, false);
  }

  if(actuatorPos.y != 0 && digitalRead(ZCAL_TEST) == HIGH)
  {
    OFFSETZ = drawLine(actuatorPos.x, 0, actuatorPos.z);
    updateActuatorPos(actuatorPos.x, 0, actuatorPos.z, false);
  }
  
  if(digitalRead(ZCAL_TEST) == LOW)
  {
    actuatorPos.z = 0;
  }
  while(actuatorPos.z != 0)
  {
    double rev = -0.001 * REV_PER_MM_Z;
    long steps = -1;
    
    ejeZ.setCurrentPosition(0);

    ejeZ.moveTo(steps);
    
    while(ejeZ.distanceToGo() != 0)
    {
      ejeZ.run();
    }
    
    if(digitalRead(ZCAL_TEST) == LOW)
    {
      actuatorPos.z = 0;
    } 
  }

  OFFSETZ = drawLine(actuatorPos.x,actuatorPos.y, 5);
  updateActuatorPos(actuatorPos.x,actuatorPos.y, 5, false);
  ejeZ.setCurrentPosition(0);

  mapaZisDef = true;
}

