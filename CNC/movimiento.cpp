#include "movimiento.hpp"
#include "calibracion.hpp"

double feedRate = 10.00;
double feedRateAnt = feedRate;
bool absMode = true;
bool useMM = true;
int spindleSpeed;
bool usilloPrendido = false;

void updateVel(double FEEDRATEPERMIN)
{
  point RelMM;
  point stepsRel;
  double FEEDRATE = FEEDRATEPERMIN * 0.0166;

  RelMM.x = FEEDRATE * REV_PER_MM_X;
  stepsRel.x = RelMM.x * STEPS_PER_REV_X;
  ejeX.setMaxSpeed(stepsRel.x);

  RelMM.y = FEEDRATE * REV_PER_MM_Y;
  stepsRel.y = RelMM.y * STEPS_PER_REV_Y;
  ejeY.setMaxSpeed(stepsRel.y);

  RelMM.z = FEEDRATE * REV_PER_MM_Z;
  stepsRel.z = RelMM.z * STEPS_PER_REV_Z;
  ejeZ.setMaxSpeed(stepsRel.z);

  feedRateAnt = feedRate;
  feedRate = FEEDRATEPERMIN;

}


void moverEje(int eje, int mult, bool cw)
{
  int multiplicador;
  int sentido = 0;
  double posiciones[3];
  double OFFSETZ;
#ifdef DEBUG  
  Serial.println(F("Movi"));
#endif
  if(cw) sentido = 1;
  if(!cw) sentido = -1;
  
  if(mult == 1)
  {
    multiplicador = 1;
  }
  if(mult == 2)
  {
    multiplicador = 10;
  }
  if(mult == 3)
  {
    multiplicador = 30;
  }
  if(eje == 1)
  {
    if(cw == true && digitalRead(esFinX2) == LOW)
    {
      return;
    }
    if(cw == false && digitalRead(esFinX1) == LOW)
    {
      return;
    }
#ifdef DEBUG
    Serial.println(F("EJE X MOVER"));
#endif
    double mm = 1 * multiplicador;
    double sent = mm * sentido;
    double rev =  sent * REV_PER_MM_X;
    long steps = rev * STEPS_PER_REV_X;
#ifdef DEBUG
    Serial.println(steps);
#endif
    OFFSETZ = drawLine(actuatorPos.x + sent, actuatorPos.y, actuatorPos.z);
    updateActuatorPos(actuatorPos.x + sent, actuatorPos.y, actuatorPos.z, false);
  }
  if(eje == 2)
  {
    if(cw == true && digitalRead(esFinY1) == LOW)
    {
      return;
    }
    if(cw == false && digitalRead(esFinY2) == LOW)
    {
      return;
    }
    double mm = 1 * multiplicador;
    double sent = mm * sentido;
    double rev = sent * REV_PER_MM_Y;
    double steps = rev * STEPS_PER_REV_Y;
    OFFSETZ = drawLine(actuatorPos.x, actuatorPos.y + sent, actuatorPos.z);
    updateActuatorPos(actuatorPos.x, actuatorPos.y + sent, actuatorPos.z, false);
  }
  if(eje == 3)
  {
    if(digitalRead(esFinZ) == LOW)
    {
      return;
    }
    double mm = 1 * multiplicador;
    double sent = mm * sentido;
    double rev = sent * REV_PER_MM_Z;
    double steps = rev * STEPS_PER_REV_Z;
    OFFSETZ = drawLine(actuatorPos.x, actuatorPos.y, actuatorPos.z + sent);
    updateActuatorPos(actuatorPos.x, actuatorPos.y , actuatorPos.z + sent, false);
  }
}

void goHome()
{
  long moverX = 99999999;
  long moverY = 99999999;
  long moverZ = 99999999;
  double OFFSETZ;


  int oldSpeedX = VELMOTPAPX;
  int oldSpeedY = VELMOTPAPY;
  int oldSpeedZ = VELMOTPAPZ;

  VELMOTPAPX = 150;
  VELMOTPAPY = 150;
  VELMOTPAPZ = 150;
  if(!esFinX1 && !esFinY1)
  {
    OFFSETZ = drawLine(-99999999,-99999999, actuatorPos.z);
    updateActuatorPos(0, 0, 0, false);
    VELMOTPAPX = oldSpeedX;
    VELMOTPAPY = oldSpeedY;
    VELMOTPAPZ = oldSpeedZ;
    return;  
    
  }
  
  if(!esFinY1)
  {
    OFFSETZ = drawLine(actuatorPos.x,-99999999, actuatorPos.z);
    updateActuatorPos(actuatorPos.x, 0, 0, false);  
  }

  if(!esFinX1)
  {
    OFFSETZ = drawLine(-99999999, actuatorPos.y, actuatorPos.z);
    updateActuatorPos(0, actuatorPos.y, 0, false);
  }
  
  VELMOTPAPX = oldSpeedX;
  VELMOTPAPY = oldSpeedY;
  VELMOTPAPZ = oldSpeedZ;
  
}

double drawLine(double x1, double y1, double z1) 
{

  if(absMode && useMM)
  {
#ifdef DEBUG
    //Serial.println(F("-----------INICIO MOVIMIENTO--------------"));
#endif
    struct point relPoint;
    struct point StepsMM;
    struct point RelMM;
    int zstep = 0;

#ifdef DEBUG

//    Serial.print(F("X: "));
//    Serial.println(x1);
//    Serial.print(F("Y: "));
//    Serial.println(y1);
//    Serial.print(F("Z: "));
//    Serial.println(z1);
//
//    Serial.print(F("ActuatorPos X: "));
//    Serial.println(actuatorPos.x);
//    Serial.print(F("ActuatorPos Y: "));
//    Serial.println(actuatorPos.y);
//    Serial.print(F("ActuatorPos Z: "));
//    Serial.println(actuatorPos.z);
//
//    Serial.print(F("ActuatorPosRel X: "));
//    Serial.println(actuatorPosRel.x);
//    Serial.print(F("ActuatorPosRel Y: "));
//    Serial.println(actuatorPosRel.y);
//    Serial.print(F("ActuatorPosRel Z: "));
//    Serial.println(actuatorPosRel.z);

#endif
    
    relPoint.x = x1-actuatorPos.x;
    relPoint.y = y1-actuatorPos.y;
    relPoint.z = z1-actuatorPos.z;


#ifdef DEBUG
//    Serial.print(F("RelPoint X: "));
//    Serial.println(relPoint.x);
//    Serial.print(F("RelPoint Y: "));
//    Serial.println(relPoint.y);
//    Serial.print(F("RelPoint Z: "));
//    Serial.println(relPoint.z);
#endif

   
    
      RelMM.z = relPoint.z * REV_PER_MM_Z;
      StepsMM.z = RelMM.z * STEPS_PER_REV_Z;

#ifdef DEBUG
      Serial.print(F("Pasos Z: "));
      Serial.println(StepsMM.z);
#endif
      
      
  
      RelMM.x = relPoint.x * REV_PER_MM_X;
      StepsMM.x = RelMM.x * STEPS_PER_REV_X;  
#ifdef DEBUG
      Serial.print(F("Pasos X: "));
      Serial.println(StepsMM.x); 
#endif

     
      RelMM.y = relPoint.y * REV_PER_MM_Y;
      StepsMM.y = RelMM.y * STEPS_PER_REV_Y;
#ifdef DEBUG
      Serial.print(F("Pasos Y: "));
      Serial.println(StepsMM.y);
#endif


      double divX = maxXDef / cantPuntDef;
      double divY = maxYDef / cantPuntDef;

      double valoX[cantPuntDef+1];
      double valoY[cantPuntDef+1];

      int puntoX;
      int puntoXAnt;

      int puntoY;
      int puntoYAnt;


      double offsetX;
      double offsetY;
      double offsetXAnt;
      double offsetYAnt;

      double promX;
      double promY;


      for(int x = 0; x <= cantPuntDef; x++)
      {
        valoX[x] = divX*x;
      }

      for(int y = 0; y <= cantPuntDef; y++)
      {
        valoY[y] = divY*y;
      }

      for(int ix = 0; ix < sizeof(valoX); ix++)
      {
        if(x1 < valoX[ix])
        {
          puntoX = ix;
          puntoXAnt = ix-1;
          break;
        }
      }

      for(int iy = 0; iy < sizeof(valoY); iy++)
      {
        if(y1 < valoY[iy])
        {
          puntoY = iy;
          puntoYAnt = iy-1;
          break;
        }
      }

      
      // offsetXAnt = table[puntoYAnt][puntoXAnt] + (table[puntoYAnt][puntoX]-table[puntoYAnt][puntoXAnt]) * x1 / (puntoX - puntoXAnt);
      // offsetX = table[puntoY][puntoXAnt] + (table[puntoY][puntoX]-table[puntoY][puntoXAnt]) * x1 / (puntoX - puntoXAnt);

      // offsetZ = offsetXAnt + (offsetX-offsetXAnt) * x1 / (puntoX - puntoXAnt);


    if(State == Printing && x1 <= maxXDef && y1 <= maxYDef && mapaZisDef == true)
    {
      offsetZAnt = offsetZ;
      offsetYAnt = table[puntoYAnt][puntoXAnt] + (table[puntoYAnt][puntoX]-table[puntoYAnt][puntoXAnt]) * (x1 - puntoXAnt*divX)/((puntoX - puntoXAnt)*divX);
      offsetY = table[puntoY][puntoXAnt] + (table[puntoY][puntoX]-table[puntoY][puntoXAnt])*(x1 - puntoXAnt * divX)/((puntoX - puntoXAnt) * divX);
      offsetZ = offsetYAnt + (offsetY-offsetYAnt)*(y1 - puntoYAnt * divY)/(puntoY*divY - puntoYAnt*divY);

#ifdef DEBUG
      Serial.print(F("X1: "));
      Serial.println(x1);

      Serial.print(F("Y1: "));
      Serial.println(y1);

      Serial.print(F("Punto X: "));
      Serial.println(puntoX);

      Serial.print(F("Punto X Ant: "));
      Serial.println(puntoXAnt);

      Serial.print(F("Punto Y: "));
      Serial.println(puntoY);

      Serial.print(F("Punto Y Ant: "));
      Serial.println(puntoYAnt);
      
      Serial.print(F("offsetYAnt: "));
      Serial.println(offsetYAnt);

      Serial.print(F("offsetY: "));
      Serial.println(offsetY);
      
      Serial.print(F("OFFSET Z: "));
      Serial.println(offsetZ);
#endif
    }
    else
    {
      offsetZAnt = offsetZ;
      offsetZ = 0;
    }

      

      

          
      
#ifdef DEBUG
    //Serial.println(F("-------------TERMINO MOVIMIENTO--------------"));
#endif

  double OFFSETZ = runEje(StepsMM.x, StepsMM.y, StepsMM.z);

    if(State == Printing)
    {
      return OFFSETZ;
    }
    else
    {
      return 0;
    }
  }

}

//Returns Z Correction

double runEje(double stepsX, double stepsY, double stepsZ)
{
  long StepsMM;
  long RelMM;
  long rel2;
  double relPointZ;
  double relPointZ2;

  long pasos[3];

  ejeX.setCurrentPosition(0);
  ejeY.setCurrentPosition(0);
  ejeZ.setCurrentPosition(0);

  pasos[X] = 0;
  pasos[Y] = 0;
  pasos[Z] = stepsZ;

  motores.moveTo(pasos);
  motores.runSpeedToPosition();

  ejeX.setCurrentPosition(0);
  ejeY.setCurrentPosition(0);
  ejeZ.setCurrentPosition(0);

  pasos[X] = stepsX;
  pasos[Y] = stepsY;
  pasos[Z] = 0;

  if(State == Printing && mapaZisDef == true)
  { 
    RelMM = (offsetZ - offsetZAnt) * long(REV_PER_MM_Z);
    StepsMM = RelMM * long(STEPS_PER_REV_Z);
    pasos[Z] = StepsMM;
  }

#ifdef DEBUG
  Serial.println(F("------ARRAY LONG-----"));
  Serial.print(F("Pasos X: "));
  Serial.println(pasos[X]);
  Serial.print(F("Pasos Y: "));
  Serial.println(pasos[Y]);
  Serial.print(F("Pasos Z: "));
  Serial.println(pasos[Z]);

#endif

  ejeX.setCurrentPosition(0);
  ejeY.setCurrentPosition(0);
  ejeZ.setCurrentPosition(0);

  motores.moveTo(pasos);
  motores.runSpeedToPosition();

  ejeX.setCurrentPosition(0);
  ejeY.setCurrentPosition(0);
  ejeZ.setCurrentPosition(0);

  return offsetZ;
}

void updateActuatorPos(double x, double y, double z, bool isRel)
{
  
    actuatorPos.x = x;
    actuatorPos.y = y;
    actuatorPos.z = z;

    actuatorPos.ejes[X] = x;
    actuatorPos.ejes[Y] = y;
    actuatorPos.ejes[Z] = z;

  return;
  
}