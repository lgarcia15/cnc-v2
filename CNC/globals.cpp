#include "globals.hpp"
#include "Pantalla.hpp"
#include "movimiento.hpp"

void(* resetFunc) (void) = 0;

unsigned long now = millis();

//-----------------Motores Paso a Paso-------------------

MultiStepper motores = MultiStepper();
AccelStepper ejeZ = AccelStepper(1,STEP_Z, DIR_Z);
AccelStepper ejeX = AccelStepper(1, STEP_X, DIR_X);
AccelStepper ejeY = AccelStepper(1, STEP_Y, DIR_Y);

// AccelStepper ejeZ(1, STEP_Z, DIR_Z);
// AccelStepper ejeX(1, STEP_X, DIR_X);
// AccelStepper ejeY(1, STEP_Y, DIR_Y);


int VELMOTPAPX = 1000;
int VELMOTPAPY = 1000;
int VELMOTPAPZ = 1000;

void setupMotores()
{
  //EJE Z
    ejeZ.setMaxSpeed(VELMOTPAPZ);
    ejeZ.setAcceleration(80000);
    ejeZ.setEnablePin(EN_Z);
    ejeZ.setPinsInverted(false,false,true);  
    ejeZ.enableOutputs();
    ejeZ.setCurrentPosition(0);
    //EJE Y
    ejeY.setMaxSpeed(VELMOTPAPY);
    ejeY.setAcceleration(80000);
    ejeY.setEnablePin(EN_Y);
    ejeY.setPinsInverted(false,false,true); 
    ejeY.enableOutputs();
    ejeY.setCurrentPosition(0);
    //EJE X 
    ejeX.setMaxSpeed(VELMOTPAPX);
    ejeX.setAcceleration(80000);
    ejeX.setEnablePin(EN_X);
    ejeX.setPinsInverted(false,false,true); 
    ejeX.enableOutputs();
    ejeX.setCurrentPosition(0);


    motores.addStepper(ejeX);
    motores.addStepper(ejeY);
    motores.addStepper(ejeZ);

}


//-------------------------------------------------------

//---------------------Archivos SD--------------------
String files[MaxFiles];
String currentFile;
String dir = "/GCODES/";
int fileLines;
int pos= -1;
int posAnt = pos;
//....................................................

//----------------Definicion de puntos----------------

struct point actuatorPos;
Ejes ejes;

//....................................................

//--------------------FINES DE CARRERA----------------
bool esFinX1 = false;
bool esFinX2 = false;
bool esFinY1 = false;
bool esFinY2 = false;
bool esFinZ = false;
//....................................................

//-----------------------ESTADOS----------------------
Estados State;
Estados StateAnt;
Estados StateDeb;
//....................................................

//-----------------Funciones Genericas-------------------

void beep()
{
  digitalWrite(44, HIGH);
#ifdef DEBUG
  Serial.println(F("Beep"));
#endif
  delay(100);
  digitalWrite(44, LOW);
}

void changeState(Estados sta)
{
  if(State == sta)
  {
    return;
  }
  
  switch(sta)
  {
    case Init:
      changeCurrentMenu(-1);
      break;
    case Home:
      changeCurrentMenu(MENUHOME);
      break;
    case SelectFile:
      changeCurrentMenu(FILES);
      break;
    case Calibration:
      changeCurrentMenu(MENUCALIBRATION);
      break;
    case Printing:
      changeCurrentMenu(MENUPRINTING);
      break;
    case SiNo:
      changeCurrentMenu(MENUSINO);
    case MapaZ:
      changeCurrentMenu(MENUMAPAZ);
    default:
      break;  
  }
  posRelAnt = posRel;
  posRel = -10;
  StateAnt = State;
  State = sta;
  need_update = true;
  return;
}


void ctrlUsillo(int acc)
{
  switch(acc)
  {
    case 3:
#ifdef DEBUG
      Serial.println(F("Prendi"));
#endif
      digitalWrite(30,HIGH);
      digitalWrite(PWM_USILLO,HIGH);
      usilloPrendido = true;
      break;
    case 4:
#ifdef DEBUG
      Serial.println(F("Prendi"));
#endif
      digitalWrite(30,HIGH);
      digitalWrite(PWM_USILLO, HIGH);
      usilloPrendido = true;
      break;
    case 5:
#ifdef DEBUG
      Serial.println(F("APAGUE"));
#endif
      digitalWrite(30,LOW);
      digitalWrite(PWM_USILLO, LOW);
      usilloPrendido = false;
      break;
  }
}

//.......................................................

