#include "fincarrera.hpp"
#include <PinChangeInterrupt.h> //Control Interrupciones




void finalCarreraX()
{
  if(digitalRead(FINX1) == LOW)
  {
      ejeX.stop();
      ejeX.run();
      ejeX.setCurrentPosition(0);
#ifdef DEBUG
    //Serial.println(F("FINX1"));
#endif
    ejeX.setCurrentPosition(0);
    ejeX.moveTo(800);
    ejeX.runSpeedToPosition();
    actuatorPos.x = 0;
    esFinX1 = true;
  }
  else
  {
    esFinX1 = false;
  }
  if(digitalRead(FINX2) == LOW)
  {
      ejeX.stop();
      ejeX.runSpeedToPosition();
      ejeX.setCurrentPosition(0);
#ifdef DEBUG
    //Serial.println(F("FINX2"));
#endif
    ejeX.setCurrentPosition(0);
    ejeX.moveTo(-800);
    ejeX.runSpeedToPosition();
    actuatorPos.x = 150;
    esFinX2 = true;
  }
  else
  {
    esFinX2 = false;
  }
  return;
    
}
void finalCarreraY()
{
   
    if(digitalRead(FINY1) == LOW)
    {
      ejeY.stop();
      ejeY.run();
      ejeY.setCurrentPosition(0);
#ifdef DEBUG
      //Serial.println(F("FINY1"));
#endif
      ejeY.setCurrentPosition(0);
      ejeY.move(800);
      ejeY.runSpeedToPosition();
      actuatorPos.y = 0;
      esFinY1 = true;
    }
    else
    {
      esFinY1 = false;
    }
    if(digitalRead(FINY2) == LOW)
    {
        ejeY.stop();
        ejeY.run();
        ejeY.setCurrentPosition(0);
#ifdef DEBUG
      //Serial.println(F("FINY2"));
#endif
      ejeY.setCurrentPosition(0);
      ejeY.move(-800);
      ejeY.runSpeedToPosition();
      actuatorPos.y = 139;
      esFinY2 = true;
    }
    else
    {
      esFinY2 = false;
    }
    return;
}
void finalCarreraZ()
{
    if(digitalRead(FINZ) == LOW)
    {
      ejeZ.stop();
      ejeZ.run();
      ejeZ.setCurrentPosition(0);
#ifdef DEBUG
      //Serial.println(F("FINZ"));
#endif
      actuatorPos.z = -9999999;
      esFinZ = true;
    }
    else 
    {
      esFinZ = false;
    }
    return;
}

