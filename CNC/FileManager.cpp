#include "FileManager.hpp"
#include "Pantalla.hpp"
#include "interfazHumana.hpp"
#include "movimiento.hpp"
#include "calibracion.hpp"



String DIR;
int nFiles;
int delayReboot = 10;
int PIN = 53;
GCodeParser GCode = GCodeParser();
SdFat sd;

File printFile;

void(* resetSoftware)(void) = 0;

void FMBegin(int pin, String dir)
{
//  if (!sd.begin(pin)) { //make sure sd card was found
//    while (true)
//    {
//      String tit = "Reseteando en " + (String)delayReboot;
//      DPerror("Error SD", tit.c_str());
//      delayReboot--;
//      delay(1000);
//      if(delayReboot == 0)
//      {
//         resetSoftware();
//      }
//    }
//  }

  PIN = pin;
  DIR = dir;
  return;
}

int fileNum()
{
  return nFiles;
}

void initSD(SdFat &sd)
{
  if (!sd.begin(PIN)) { //make sure sd card was found
    while (true)
    {
      String tit = "Reseteando en " + (String)delayReboot;
      DPerror("Error SD", tit.c_str());
      delayReboot--;
      delay(1000);
      if(delayReboot == 0)
      {
          resetSoftware();
      }
    }
  }
}

void rebootFM(int pin, String dir)
{
  // if (!sd.begin(pin)) { //make sure sd card was found
  //  while (true)
  //  {
  //    String tit = "Reseteando en " + (String)delayReboot;
  //    DPerror("Error SD", tit.c_str());
  //    delayReboot--;
  //    delay(1000);
  //    if(delayReboot == 0)
  //    {
  //       resetSoftware();
  //    }
  //  }

}

int getLines(String file)
{
  SdFat sd;
  initSD(sd);
  int num = 0;
  File arch = sd.open(file);
  char line[256];
  char c;
  int lineIndex = 0;
  if(!arch) 
  {
#ifdef DEBUG
    Serial.println(F("ERROR AL ABRIR ARCHIVO"));
#endif
  }
  
  DPcargando();
  while(arch.available())
  {
      c = arch.read();
      if (c == '\n')
      {
        num++;
      } 
      if(c == '\r')
      {
        if(arch.read() == '\n')
        {
          num++;
        }
      }
      

      
  } 
  arch.close();
  return num;
}

void getFiles(String * files)
{
  
 SdFat sd;
 initSD(sd);
 nFiles = 0;
 File folder = sd.open(DIR.c_str());
 while(true){
#ifdef DEBUG
   Serial.println(F("Aca"));
#endif
   File entry = folder.openNextFile();
   if(!entry){
     folder.rewindDirectory();
     break;
   }else{
   nFiles++;
   }  
   entry.close();
 }
 
 for(int i = 0; i < nFiles; i++)
 {
   char fileName[60];
   File entry = folder.openNextFile();
   entry.getName(fileName,60);
#ifdef DEBUG
   Serial.println(fileName);
#endif
   files[i] = String(fileName);
   entry.close();  
 }

 folder.close();
  
}

void openPrintFile(SdFat &sd)
{
  printFile = sd.open(currentFile);
  while(!printFile)
  {
    DPerror("Error2 al", "Abrir el archivo");
    closePrintFile();
    openPrintFile(sd);
  }
}

void closePrintFile()
{
  printFile.close();
}

void execLines()
{
  unsigned long now = millis();
  
  SdFat sd;
  initSD(sd);
  openPrintFile(sd);
 
  while (printFile.available()) 
  {
      now = millis();
      ISR_ENC(now);
      DPTick();
      if(State != Printing) break;

      if (GCode.AddCharToLine(printFile.read()))
      {
        char * line = GCode.line;
#ifdef DEBUG
        Serial.println(line);
#endif
        GCode.ParseLine();
        currentLine++;
        printProgress = ((double)currentLine / (double)fileLines); 
        if(printProgress == 1)
        {
          oldProgress = 0;
          printProgress = 0;
          changeState(Home);
        }
        processIncomingLine();
      }
  }

  closePrintFile();
}



void processIncomingLine() 
{
  
    double OFFSETZ;
    struct point newPos;
    newPos.x = 0;
    newPos.y = 0;
    newPos.z = 0;
    
    double tempFeedRate;
    double mmPs;
    double revPmm;
    double stepPmm;
    int tempSpindleSpeed;
    
    if(GCode.HasWord('G'))
    {
      switch((int)GCode.GetWordValue('G'))
      {
        case 0:
        case 1:
          if(GCode.HasWord('F'))
          {
           tempFeedRate = GCode.GetWordValue('F');
           if(tempFeedRate != feedRate)
           {
             updateVel(tempFeedRate*multFEED);
             need_update = true;
           }
          }
          if(GCode.HasWord('X'))
          {
            newPos.x = GCode.GetWordValue('X');
          }
          else
          {
            newPos.x = actuatorPos.x;
          }
          if(GCode.HasWord('Y'))
          {
            newPos.y = GCode.GetWordValue('Y');
          }
          else
          {
            newPos.y = actuatorPos.y;
          }
          if(GCode.HasWord('Z'))
          {
            newPos.z = GCode.GetWordValue('Z');
          }
          else
          {
            newPos.z = actuatorPos.z;
          }
          
          OFFSETZ = drawLine(newPos.x, newPos.y, newPos.z);
          updateActuatorPos(newPos.x, newPos.y, newPos.z, true);
          break;
        case 2:
          if(GCode.HasWord('F'))
          {
            tempFeedRate = GCode.GetWordValue('F');
            if(tempFeedRate != feedRate)
            {
              updateVel(tempFeedRate*multFEED);
              need_update = true;
            }
          }
          arcToSegments(true);
          break;
        case 3:
          if(GCode.HasWord('F'))
          {
            tempFeedRate = GCode.GetWordValue('F');
            if(tempFeedRate != feedRate)
            {
              updateVel(tempFeedRate*multFEED);
              need_update = true;
            }         
          }
          arcToSegments(false);
          break;
        case 20:
          useMM = false;
          need_update = true;
          break;
        case 21:
          useMM = true;
          need_update = true;
          break;
        case 28:
          goHome();
          break;
        case 90:
          absMode = true;
          need_update = true;
          break;
        case 91:
          absMode = false;
          need_update = true;
          break;
    
        default:
          break;
    }
      
      
      
    }
    else if(GCode.HasWord('M'))
    {
      switch((int)GCode.GetWordValue('M'))
      {
            case 2:
#ifdef DEBUG
              Serial.println(F("Termino Programa"));
#endif
              break;
            case 3:
              ctrlUsillo(3);
              break;
            case 4:
              ctrlUsillo(4);
              break;
            case 5: 
              ctrlUsillo(5);
              break;
            default:
#ifdef DEBUG
              Serial.println(F("Command not recognized : M"));
#endif
              break;
      }
    }
    else if(GCode.HasWord('F'))
    {
      tempFeedRate = GCode.GetWordValue('F');
      if(tempFeedRate != feedRate)
      {
        updateVel(tempFeedRate * multFEED);
        need_update = true;
      }
    }
    else if(GCode.HasWord('S'))
    {
      tempSpindleSpeed = (int)GCode.GetWordValue('S');
      if(tempSpindleSpeed != spindleSpeed)
      {
        spindleSpeed = tempSpindleSpeed;
        need_update = true;
      }
      
    }


}


void arcToSegments(bool isCW)
{
  double toX;
  double toY;
  double i;
  double j;
  double tempFeedRate;
  double mmPs;
  double revPmm;
  double stepPmm;
  int tempSpindleSpeed;
  double OFFSETZ;
  //https://marlinfw.org/docs/gcode/G002-G003.html
  double maxSegmentLen = 1;
  double maxSegmentAngle = ((double)5 / (double)180) * PI;        

  if(GCode.HasWord('X'))
  {
    if(absMode)
    {
      toX = GCode.GetWordValue('X');
    }
    else
    {
      toX = actuatorPos.x + GCode.GetWordValue('X');
    }
  }
  else
  {
#ifdef DEBUG
    Serial.println(F("Error G02/G03 No hay X"));
#endif
    return;
  }
  if(GCode.HasWord('Y'))
  {
    if(absMode)
    {
      toY = GCode.GetWordValue('Y');
    }
    else
    {
      toY = actuatorPos.x + GCode.GetWordValue('Y');
    }
  }
  else
  {
#ifdef DEBUG            
    Serial.println(F("Error G02/G03 No hay Y"));
#endif
    return;
  }
  if(GCode.HasWord('I'))
  {
    i = GCode.GetWordValue('I');
  }
  else
  {
#ifdef DEBUG
    Serial.println(F("Error G02/G03 No hay I"));
#endif
    return;
  }
  if(GCode.HasWord('J'))
  {
    j = GCode.GetWordValue('J');
  }
  else
  {
#ifdef DEBUG
    Serial.println(F("Error G02/G03 No hay J"));
#endif
    return;
  }

  if(GCode.HasWord('F'))
  {
    
    tempFeedRate = GCode.GetWordValue('F');
    if(tempFeedRate != feedRate)
    {
      updateVel(tempFeedRate * multFEED);
      need_update = true;
    }
    
  }
  double fromX = actuatorPos.x;
  double fromY = actuatorPos.y;


  double rad = sqrt(i * i + j * j);

  double cX = fromX + i;
  double cY = fromY + j;

  double startAngle = atan2(fromY - cY, fromX - cX);
  double endAngle = atan2(toY - cY, toX - cX);

  double aX = fromX - cX;
  double aY = fromY - cY;
  double bX = toX - cX;
  double bY = toY - cY;
  // https://stackoverflow.com/questions/40286650/how-to-get-the-anti-clockwise-angle-between-two-2d-vectors
  double ccwAngle = atan2(aX * bY - aY * bX, aX * bX + aY * bY);
  if (ccwAngle < 0) ccwAngle += PI * 2;

  double totalDeltaAngle = ccwAngle;
  if (isCW) totalDeltaAngle = totalDeltaAngle - PI * 2;


  double bowLen = abs(totalDeltaAngle) * rad;
  // how many segments needs the bow to be divided into

  double requiredSegmentsByLengthLimit = ceil(
    bowLen / maxSegmentLen
  );
  double requiredSegmentsByAngleLimit = ceil(
    abs(totalDeltaAngle) / maxSegmentAngle
  );

  double requiredSegments = max(
    requiredSegmentsByLengthLimit,
    requiredSegmentsByAngleLimit
  );



  

  double deltaAngle = totalDeltaAngle / requiredSegments;

  double deltaSegmentLen = maxSegmentAngle;
  if (deltaAngle > maxSegmentAngle) {
    // angle between to big for set limits, make smaller segments
    deltaAngle = maxSegmentAngle;
    deltaSegmentLen = deltaAngle * rad;
    requiredSegments = ceil(bowLen / deltaSegmentLen);
  }

  String output = "";
  if (requiredSegments > 1) {
    for (int cont = 1; cont < requiredSegments; cont++) {
      double angle = startAngle + cont * deltaAngle;
      double segX;
      double segY;
      if(absMode)
      {
        segX = (cX + cos(angle) * rad);
        segY = (cY + sin(angle) * rad);
      }
      else
      {
        segX = (cos(angle) * rad);
        segY = (sin(angle) * rad);
      }
      
      OFFSETZ = drawLine(segX, segY, actuatorPos.z);
      updateActuatorPos(segX, segY, actuatorPos.z, true);
    }
  }

}
