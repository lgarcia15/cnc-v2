#ifndef _MOVIMIENTO_H
#define _MOVIMIENTO_H

#include "globals.hpp"

extern double feedRate;
extern double feedRateAnt;
extern bool absMode;
extern bool useMM;
extern int spindleSpeed;
extern bool usilloPrendido;


void moverEje(int eje, int mult, bool cw);
void goHome();
double drawLine(double x1, double y1, double z1);
double runEje(double stepsX, double stepsY, double stepsZ);
void updateActuatorPos(double x, double y, double z, bool isRel);
void updateVel(double FEEDRATE);


#endif