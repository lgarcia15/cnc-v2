/* 
 Mini CNC Plotter firmware, based in TinyCNC https://github.com/MakerBlock/TinyCNC-Sketches
 Send GCODE to this Sketch using gctrl.pde https://github.com/damellis/gctrl
 Convert SVG to GCODE with MakerBot Unicorn plugin for Inkscape available here https://github.com/martymcguire/inkscape-unicorn
 
 More information about the Mini CNC Plotter here (german, sorry): http://www.makerblog.at/2015/02/projekt-mini-cnc-plotter-aus-alten-cddvd-laufwerken/
  */


#include "Pantalla.hpp" // Archivo para controlar la pantalla
#include "FileManager.hpp" // Archivo para controlar la SD
#include "movimiento.hpp"
#include "calibracion.hpp"
#include "globals.hpp"
#include "fincarrera.hpp"
#include "interfazHumana.hpp"
#include <PinChangeInterrupt.h> //Control Interrupciones

/**********************
 * void setup() - Initialisations
 ***********************/
void setup() {

  //  Setup
#ifdef DEBUG
  Serial.begin( 9600 ); 
#endif
 //INICIO DP
 DPBegin(12); //Funcion que inicia el DP (Display)
 changeState(Init);
 DPinit();

 ejeCal = 1;
 multCal = 1;

 actuatorPos.z = 0;

  
 //INICIOSD
 FMBegin(53, dir); // Inicio la SD, ver Archivo FileManager.hpp
 getFiles(files); // Inicio leo los archivos y le paso un puntero de mi array, ver Archivo FileManager.hpp
 int nFiles2 = fileNum(); // Obtengo la cantidad de archivos leidos, ver Archivo FileManager.hpp

 int delayReboot = 10;

 //SETEO LA CANTIDAD DE MENUES SEGUN CANT DE ARCHIVOS
 int cantM = nFiles2/4;
 bool hayResto = nFiles2%4;
 if(hayResto)
 {
    cantM++;
 }

 cantM += 4;


 if(digitalRead(FINX1) == LOW) esFinX1 = true;
 if(digitalRead(FINX2) == LOW) esFinX2 = true;
 if(digitalRead(FINY1) == LOW) esFinY1 = true;
 if(digitalRead(FINY2) == LOW) esFinY2 = true;
 if(digitalRead(FINZ) == LOW) esFinZ = true;

 
 DPSetMenuQ(cantM);

  loadFilesToDP();
  //ENCODER
  initEncoder();
  initBotones();


  //BEEPER
  pinMode(beeper, OUTPUT);
  digitalWrite(beeper, LOW);
  
  //USILLO
  pinMode(30, OUTPUT);
  digitalWrite(30, LOW);
  pinMode(PWM_USILLO, OUTPUT);
  digitalWrite(PWM_USILLO,LOW);
  usilloPrendido = false;

  //CONFIGURACION CONTINUIDAD
  pinMode(ZCAL_TEST, INPUT_PULLUP);


  //CONFIGURACION FINES DE CARRERA
  //EJE X
  pinMode(FINX1, INPUT_PULLUP);
  pinMode(FINX2, INPUT_PULLUP);
  attachPCINT(digitalPinToPinChangeInterrupt(FINX1),finalCarreraX,CHANGE);
  attachPCINT(digitalPinToPinChangeInterrupt(FINX2),finalCarreraX,CHANGE);
  //EJE Y
  pinMode(FINY1, INPUT_PULLUP);
  pinMode(FINY2, INPUT_PULLUP);
  attachPCINT(digitalPinToPinChangeInterrupt(FINY1),finalCarreraY,CHANGE);
  attachPCINT(digitalPinToPinChangeInterrupt(FINY2),finalCarreraY,CHANGE);
  //EJE Z
  pinMode(FINZ, INPUT_PULLUP);
  attachPCINT(digitalPinToPinChangeInterrupt(FINZ),finalCarreraZ,CHANGE);

  setupMotores();

  delay(2000);
  DPinit2();
  delay(2000);
  goHome();
  changeState(Home);
}


/**********************
 * void loop() - Main loop
 ***********************/
void loop() 
{
  
  char c;
  int delayReboot = 10;
  actuatorPos.z = 0;
  unsigned long now = millis();
  
  
  while (1) 
  {
    now = millis();
    ISR_ENC(now);
    DPTick();

#ifdef DEBUG
    //Serial.println(analogRead(A1));
#endif

    if(State==Calibration)
    {
     
    }
    if(State == Printing)
    {
      currentLine = 0;
      oldProgress = 0;
      need_update = true;
      DPTick(); 
      execLines();
    }
    
  }
}



