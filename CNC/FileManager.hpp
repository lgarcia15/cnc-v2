#ifndef _FILEMANAGER_H_
#define _FILEMANAGER_H_
#include <Arduino.h>
#include <SPI.h>
#include <SdFat.h>
#include <GCodeParser.h> //Lectura codigo G
#include "globals.hpp"

extern int nFiles;

void FMBegin(int pin, String dir);
void getFiles(String * files);
int fileNum();
void rebootFM(int pin, String dir);
int getLines(String file);
void openPrintFile();
void closePrintFile();
void execLines();
void arcToSegments(bool isCW);
void processIncomingLine();
void arcToSegments(bool isCW);


extern String DIR;
extern File printFile;
extern int PIN;



#endif
