#ifndef _INTERFAZ_HUMANA_H
#define _INTERFAZ_HUMANA_H
#include "globals.hpp"
#include <InputDebounce.h> //Control Botones
#include "Rotary.h" //Control Encoder

#define BUTTON_DEBOUNCE_DELAY   20
#define BUTTON_MAS 26
#define BUTTON_MENOS 28
static InputDebounce buttonMas; // not enabled yet, setup has to be called first, see setup() below
static InputDebounce buttonMenos;

#define encCLK A14
#define encDT A15
#define btnENC A8
#define btnStop 46

extern bool ok;


void buttonMas_pressedCallback(uint8_t pinIn);
void buttonMenos_pressedCallback(uint8_t pinIn);
void ISR_ENC(unsigned long Now);
void initBotones();
void initEncoder();
bool leerStop();
int leerBoton();
int getLado();



#endif