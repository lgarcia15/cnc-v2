#include "interfazHumana.hpp"
#include "movimiento.hpp"
#include "Pantalla.hpp"
#include "calibracion.hpp"
#include "FileManager.hpp"



Rotary r = Rotary(encCLK, encDT);
bool ok = false;

void buttonMas_pressedCallback(uint8_t pinIn)
{
#ifdef DEBUG
  Serial.println("Callback Mas");
#endif
  if(State == Calibration) moverEje(ejeCal, multCal, true);

  if(State == MapaZ && posRel == 3 && posRel < 999)
  {
    puntosCal++;
    need_update = true;
  }

  if(State == Printing && posRel == 1)
  {
    multFEED += 0.1;
    updateVel(feedRate*multFEED);
    need_update = true;
  }
  
}

void buttonMenos_pressedCallback(uint8_t pinIn)
{
#ifdef DEBUG
  Serial.println("Callback Menos");
#endif
  if(State == Calibration) moverEje(ejeCal, multCal, false);
  if(State == MapaZ && posRel == 3 && posRel > 1)
  {
    puntosCal--;
    need_update = true;
  }

  if(State == Printing && posRel == 1)
  {
    multFEED -= 0.1;
    updateVel(feedRate*multFEED);
    need_update = true;
  }
}

void initBotones()
{
  buttonMas.registerCallbacks(buttonMas_pressedCallback,NULL,NULL,NULL);
  buttonMenos.registerCallbacks(buttonMenos_pressedCallback, NULL,NULL,NULL);
  buttonMas.setup(BUTTON_MAS, BUTTON_DEBOUNCE_DELAY, InputDebounce::PIM_INT_PULL_UP_RES);
  buttonMenos.setup(BUTTON_MENOS, BUTTON_DEBOUNCE_DELAY, InputDebounce::PIM_INT_PULL_UP_RES);
}

void initEncoder()
{
  pinMode(btnENC, INPUT_PULLUP);
  pinMode(btnStop, INPUT_PULLUP);
  r.begin(true);
}

void ISR_ENC(unsigned long Now)
{ 
  now = millis();
  buttonMas.process(now);
  buttonMenos.process(now);
  if(State == MapaZ)
  {
      int lado = getLado();
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        need_update = true;
      }
      if (lado == 1) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        if(posRel < 4)
        {
          posRelAnt = posRel;
          posRel++;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }
      if (lado == 2) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        else if(posRel > 1)
        {
          posRelAnt = posRel;
          posRel--;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }

      if(leerBoton())
      {
        switch(posRel)
        {
          case 1:
            
            break;
          case 2:
            maxX = actuatorPos.x;
            maxY = actuatorPos.y;
#ifdef DEBUG
            Serial.print(F("MAX X: "));
            Serial.println(maxX);
            Serial.print(F("MAX Y: "));
            Serial.println(maxY);
#endif
            beep();
            break;
          case 3:
            break;
          case 4:          
            mapaZ(puntosCal, maxX, maxY);
            break;
        }
        
      }

      if(leerStop())
      {
        changeState(Calibration);
        need_update = true;
        beep();
      }
      
  }
  
  
  if(State == Calibration)
  {
    int lado = getLado();
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        need_update = true;
      }
      if (lado == 1) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        if(posRel < 9)
        {
          posRelAnt = posRel;
          posRel++;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }
      if (lado == 2) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        else if(posRel > 1)
        {
          posRelAnt = posRel;
          posRel--;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }

      if(leerBoton())
      {
        switch(posRel)
        {
          case 1:
            if(ejeCal  < 3)
            {
              ejeCal ++;
            }
            else
            {
              ejeCal = 1;
            }
            need_update = true;
            break;
          case 2:
            if(multCal  < 3)
            {
              multCal ++;
            }
            else
            {
              multCal = 1;
            }
            need_update = true;
            break;
          case 3:
            actuatorPos.x = 0;
            actuatorPos.y = 0;
            actuatorPos.z = 0;
            need_update = true;
            break;
          case 6:          
            if(!usilloPrendido)
            {
              ctrlUsillo(3);
            }
            else
            {
              ctrlUsillo(5);
            }
            break;
          case 7:
            setZeroZ();
            break;
          case 8:
            changeState(MapaZ);
            beep();
            need_update = true;
            break;
          case 9:
            goHome();
            break;
        }
        beep(); 
      }
      
      if(leerStop())
      {
        changeState(Home);
        beep();
      }
  }
 
  if(State == SiNo)
  {
#ifdef DEBUG
      //Serial.println(posRel);
#endif
      int lado = getLado();
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
      }
      if (lado == 1) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        if(posRel <= 2)
        {
          posRelAnt = posRel;
          posRel++;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }
      if (lado == 2) 
      {
        if(posRel == -10)
        {
          posRelAnt = posRel;
          posRel = 1;
        }
        else if(posRel > 1)
        {
          posRelAnt = posRel;
          posRel--;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        return;
      }

      if(leerBoton())
      {
        beep();
        if(posRel == 2)
        {
          changeState(StateAnt);
        }
        if(posRel == 1)
        {
          //rebootFM(53, dir);
          changeState(Printing);
          fileLines = getLines(currentFile);
#ifdef DEBUG
          Serial.println(fileLines);
#endif         
          //openPrintFile();
          
        }
      }
  }
  if(State == SelectFile)
  {
      int lado = getLado();
      if(pos == -1)
      {
        posAnt = pos;
        pos = 0;
        posRel = (pos%4) + 1;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      if (lado == 1) 
      {
        if(pos == -1)
        {
          posAnt = pos;
          pos = 0;
          posRel = (pos%4) + 1;
          if(posRel != posRelAnt)
          {
            need_update = true;
          }
          else
          {
            need_update = false;
          }
          return;
        }
        else if(pos < fileNum()-1)
        {
          posAnt = pos;
          pos++;
          posRel = (pos%4) + 1;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      if (lado == 2) 
      {
        if(pos == -1)
        {
          posAnt = pos;
          pos = 0;
          posRel = (pos%4) + 1;
          if(posRel != posRelAnt)
          {
            need_update = true;
          }
          else
          {
            need_update = false;
          }
          return;
        }
        else if(pos > 0)
        {
          posAnt = pos;
          pos--;
          posRel = (pos%4) + 1;
        }
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }

      if(leerBoton())
      {
        currentFile = dir + files[pos];
#ifdef DEBUG
        Serial.println(currentFile);
#endif
        beep();
        need_update = true;
        changeState(SiNo);
      }
#ifdef DEBUG
      //Serial.println(leerBoton());
      //Serial.println(leerStop());
#endif
      if(leerStop())
      {
        changeState(Home);
        need_update = true;
        beep();
      }
    
      
  }

  if(State == Home)
  {
    int lado = getLado();
    if(posRel == -10)
    {
      posRelAnt = posRel;
      posRel = 1;
      if(posRel != posRelAnt)
      {
        need_update = true;
      }
      else
      {
        need_update = false;
      }
      return;
    }
    if (lado == 1) 
    {
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      else if(posRel < 3)
      {
        posRelAnt = posRel;
        posRel++;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
      }
      return;
    }
    if (lado == 2) 
    {
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      else if(posRel > 1)
      {
        posRelAnt = posRel;
        posRel--;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
      }
      return;
    }
    if(leerBoton())
    {
      if(posRel == 1)
      {
        changeState(SelectFile);
        need_update = true;
        beep();
      }
      if(posRel == 2)
      {
        changeState(Calibration);
        need_update = true;
        beep();
      }
      if(posRel == 3)
      {
        beep();
        resetFunc();
      }
      
    }
  }
  if(State == Printing)
  {
    int lado = getLado();
    if(posRel == -10)
    {
      posRelAnt = posRel;
      posRel = 1;
      if(posRel != posRelAnt)
      {
        need_update = true;
      }
      else
      {
        need_update = false;
      }
      return;
    }
    if (lado == 1) 
    {
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      else if(posRel < 1)
      {
        posRelAnt = posRel;
        posRel++;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
      }
      return;
    }
    if (lado == 2) 
    {
      if(posRel == -10)
      {
        posRelAnt = posRel;
        posRel = 1;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
        return;
      }
      else if(posRel > 1)
      {
        posRelAnt = posRel;
        posRel--;
        if(posRel != posRelAnt)
        {
          need_update = true;
        }
        else
        {
          need_update = false;
        }
      }
      return;
      if(leerStop())
      {
        beep();
        printProgress = 0;
        oldProgress = 0;
        ctrlUsillo(5);
        changeState(Home);
        need_update = true;
      }
    }
  }
}


bool leerStop()
{
  stopBtn = digitalRead(46);
  bool res;
  if(stopBtn == 0 && stopBtnAnt == 1)
  {
    stopBtnAnt = stopBtn;
    res = true;
  }
  else
  {
    stopBtnAnt = stopBtn;
    res = false;
  }
  return res;
}

int leerBoton()
{
    boton = digitalRead(A8);
    bool res;    
    if (boton == 0 && botonAnt == 1) 
    {
      botonAnt = boton;
#ifdef DEBUG
      //Serial.println(F("ACA"));
#endif
      res = true;
      
    }
    else
    {
      botonAnt = boton;
      res = false;
    }
    return res;
}

int getLado()
{
      int lado = 0;
      unsigned char result = r.process();
      if(result)
      {
        if(result == DIR_CW)
        {
          lado = 1;
        }
        else
        {
          lado = 2;
        }
      }
      return lado;
      
}