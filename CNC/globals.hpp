#ifndef _GLOBALS_H
#define _GLOBALS_H

//#include <BasicStepperDriver.h> //Control Motores PAP
#include <MultiStepper.h>
#include <AccelStepper.h> //Control Motores PAP



//------------Definicion de estructuras---------------
struct point { 
  double x; 
  double y; 
  double z; 
  double ejes[3];

};
//....................................................

//----------------Definicion de Enum------------------
enum Estados
{
  None,
  Init,
  Home,
  SelectFile,
  Calibration,
  Printing,
  SiNo,
  MapaZ
};

enum Ejes
{
  X,
  Y,
  Z
};
//....................................................

//----------------DEFINICIONES DISPLAY----------------
#define NONE -1
#define HOME 1
#define FILES 2
#define PRINTING 3
#define CALIBRATING 4

#define MENUHOME 0
#define MENUCALIBRATION 1
#define MENUPRINTING 2
#define MENUSINO 3
#define MENUMAPAZ 4
//....................................................

//-------------------Motores Paso a Paso--------------

//EJE Z
#define EN_Z 5
#define STEP_Z 4
#define DIR_Z 3
#define STEPS_PER_REV_Z 1600
#define REV_PER_MM_Z 0.125
//EJE X
#define EN_X 8
#define STEP_X 7
#define DIR_X 6
#define STEPS_PER_REV_X 1600
#define REV_PER_MM_X 0.125
//EJE Y
#define EN_Y 9
#define STEP_Y 10
#define DIR_Y 11
#define STEPS_PER_REV_Y 1600
#define REV_PER_MM_Y 0.125

extern MultiStepper motores;

extern AccelStepper ejeZ;
extern AccelStepper ejeX;
extern AccelStepper ejeY;

extern int VELMOTPAPX;
extern int VELMOTPAPY;
extern int VELMOTPAPZ;

//....................................................





//----------------Definicion de puntos----------------

extern struct point actuatorPos;
extern Ejes ejes;

//....................................................

//------------------Control Usillo--------------------
#define PWM_USILLO 2
//....................................................



//---------------Definicion de estados----------------
extern Estados State;
extern Estados StateAnt;
extern Estados StateDeb;
//....................................................

//-----------------------Beeper-----------------------
#define beeper 44
//....................................................

//----------------------TIMER-------------------------
extern unsigned long now;
//....................................................


//-----------------Funciones Genericas----------------
void changeState(Estados sta);
void ctrlUsillo(int acc);
void beep();
void setupMotores();
extern void(* resetFunc) (void);//declare reset function at address 0
//....................................................

//--------------------FINES DE CARRERA----------------
#define FINX1 A9
#define FINX2 A10
#define FINY1 A11
#define FINY2 A12
#define FINZ A13

extern bool esFinX1;
extern bool esFinX2;
extern bool esFinY1;
extern bool esFinY2;
extern bool esFinZ;
//....................................................


//---------------------Archivos SD--------------------
#define MaxFiles 100
extern String files[MaxFiles];
extern String currentFile;
extern String dir;
extern int fileLines;
extern int pos;
extern int posAnt;
//....................................................


#endif