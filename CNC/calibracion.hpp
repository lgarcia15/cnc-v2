#ifndef _CALIBRACION_H
#define _CALIBRACION_H

#include "globals.hpp"
#define ZCAL_TEST 24

void setZeroZ();
void mapaZ(int cantPunt, double maxX, double maxY);

extern int puntosCal;
extern double maxX;
extern double maxY;
extern double maxXDef;
extern double maxYDef;
extern double offsetZ;
extern double offsetZAnt;
extern bool mapaZisDef;
extern int cantPuntDef;
extern int ejeCal;
extern int multCal;
extern double multFEED;
extern double **table;


#endif