#ifndef _PANTALLA_H_
#define _PANTALLA_H_

#include <WString.h>                // include the String library
//extern U8G2_ST7920_128X64_F_SW_SPI *u8g2;



typedef String dataDef[8];

struct pant
{
  dataDef txt;
  int hili;
};
bool DPDrawMenu(int menu);
void DPBegin(int separacion);
int DPgetMenuQ();
void DPloadData(int menu, int pos, String str, int hili, int type);
void DPSetMenuQ(int menuQ);
void DPinit();
void DPinit2();
void DPcargando();
void DPMapaZ(int posRel, int puntosCal);
void DPhome();
void DPerror(char * errL1, char * errL2);
void DPimprimiendo(double progress, double feedRate, bool useMM, bool absMode, double vMot, double multF);
void progressBar(int x, int y, int w, int h, double value);
void DPsiNo(bool sel, char * title);
void DPselectFile(int pos);
void DPcalibration(int eje, double x0, double y0, double z0, int mult, bool isMM, int pos);
void DPTick();
void updateFilesDP(int Menu, int hili);
void loadFilesToDP();
void changeCurrentMenu(int Menu);

extern int currentMenu;
extern bool need_update;
extern int posRel;
extern int posRelAnt;
extern int stopBtn;
extern int stopBtnAnt;
extern int boton;
extern int botonAnt;
extern double printProgress;
extern double oldProgress;
extern int currentLine;
  //(U8G2_R0, /* clock=*/ 13, /* data=*/ 22, /* CS=*/ 12, /* reset=*/ 12); // Feather HUZZAH ESP8266, E=clock=14, RW=data=13, RS=CS  
    



#endif
